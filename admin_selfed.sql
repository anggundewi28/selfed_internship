# MySQL-Front 5.1  (Build 4.13)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: localhost    Database: admin_selfed
# ------------------------------------------------------
# Server version 5.5.16-log

#
# Source for table ads
#

DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_name` varchar(115) NOT NULL,
  `ad_url` varchar(145) DEFAULT NULL,
  `ad_path` varchar(124) NOT NULL,
  `ad_status` tinyint(1) NOT NULL,
  `ad_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Dumping data for table ads
#

LOCK TABLES `ads` WRITE;
/*!40000 ALTER TABLE `ads` DISABLE KEYS */;
INSERT INTO `ads` VALUES (2,'ssss','asdfdas','',0,'2018-06-12 09:11:19');
INSERT INTO `ads` VALUES (5,'dsfasf','sdfasdf','http://localhost/selfed-service/assets/uploads/coollogo_com-19230201.png',1,'2018-06-12 09:18:22');
INSERT INTO `ads` VALUES (6,'tewt','google.com','http://localhost/selfed-service/assets/uploads/',1,'2018-06-21 07:49:17');
/*!40000 ALTER TABLE `ads` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table articles
#

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_title` varchar(125) NOT NULL,
  `article_cover` varchar(111) DEFAULT NULL,
  `article_content` text NOT NULL,
  `article_author` int(11) NOT NULL,
  `article_status` int(1) NOT NULL,
  `article_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Dumping data for table articles
#

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'test',NULL,'saa\n',0,1,'2018-06-21 08:54:48');
INSERT INTO `articles` VALUES (5,'test',NULL,'0',0,1,'2018-06-22 09:04:34');
INSERT INTO `articles` VALUES (7,'','./assets/uploads/',' ',0,1,'2018-12-10 14:40:27');
INSERT INTO `articles` VALUES (8,'','./assets/uploads/',' ',0,1,'2018-12-10 14:40:30');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table category
#

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

#
# Dumping data for table category
#

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Komputer Perangkat Lunak',1);
INSERT INTO `category` VALUES (2,'Komputer Perangkat Keras',1);
INSERT INTO `category` VALUES (3,'Komputer Jaringan',1);
INSERT INTO `category` VALUES (4,'Pemodelan 3 Dimensi',1);
INSERT INTO `category` VALUES (5,'Animasi 3 Dimensi',1);
INSERT INTO `category` VALUES (11,'a1',1);
INSERT INTO `category` VALUES (13,'aab',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table classes
#

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(111) NOT NULL,
  `class_educator` int(11) NOT NULL,
  `class_members` varchar(124) NOT NULL,
  `class_status` tinyint(1) NOT NULL,
  `class_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table classes
#

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table member
#

DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL,
  `regdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

#
# Dumping data for table member
#

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'cliff.ririmasse@gmail.com','qwerty','Cliff Michael','08523656752',1,'2018-12-04 10:13:16');
INSERT INTO `member` VALUES (2,'josh2003ching@hotmail.com','password','Josh','90719993',1,'2018-12-03 13:21:02');
INSERT INTO `member` VALUES (3,'liss4444@yahoo.com','P@ssword123','Nur Sulistyowati ','085640086737',1,'2018-12-04 10:13:27');
INSERT INTO `member` VALUES (4,'fitra90@gmail.com','openselfed','Fitra Fadilana','085867376832',0,'2018-12-04 10:13:40');
INSERT INTO `member` VALUES (6,'anggundewi28@gmail.com','aaa','anggun','089646380263',1,'2018-12-04 10:13:44');
INSERT INTO `member` VALUES (16,'anggundewi28@gmail.com','aaa','aaa123','089646380263',1,'2018-12-04 04:14:22');
INSERT INTO `member` VALUES (18,'anggundewi28@yahoo.com','aaa','angguna','089646380263',1,'2018-12-07 14:38:36');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table message_rooms
#

DROP TABLE IF EXISTS `message_rooms`;
CREATE TABLE `message_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_members` varchar(125) NOT NULL,
  `room_status` tinyint(1) NOT NULL,
  `room_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table message_rooms
#

LOCK TABLES `message_rooms` WRITE;
/*!40000 ALTER TABLE `message_rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_rooms` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table messages
#

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_room` int(11) NOT NULL,
  `message_sender` int(11) NOT NULL,
  `message_receiver` int(11) NOT NULL,
  `message_text` text NOT NULL,
  `message_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table messages
#

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table modules
#

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_member` int(255) NOT NULL,
  `id_cat` int(255) NOT NULL,
  `path` longtext NOT NULL,
  `thumb` longtext NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `pembayaran` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

#
# Dumping data for table modules
#

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,1,0,'','','Dagelan Bom Ayam','Video apps','',0,'2018-05-21 07:38:50');
INSERT INTO `modules` VALUES (2,1,0,'','','Bom ayam','Video dagelan','',0,'2018-05-21 07:44:08');
INSERT INTO `modules` VALUES (3,1,0,'http://selfed.co/selfed_services/modules/selfed-cliff.ririmasse@gmail.com-5V35E0X2UKDCRAUQED4ZJ3NSVBSXTDNOYGK.mp4','http://selfed.co/selfed_services/modules/thumbs/selfed-cliff.ririmasse@gmail.com-5V35E0X2UKDCRAUQED4ZJ3NSVBSXTDNOYGK.jpg','Don\\\'t Use Cellphone While Driving','Safety First When Driving.','',0,'2018-05-21 08:09:16');
INSERT INTO `modules` VALUES (4,1,0,'http://selfed.co/selfed_services/modules/selfed-cliff.ririmasse@gmail.com-377VRHZUYZFE3I9SR36AO8SBRVP6KWGDTDZ.mp4','http://selfed.co/selfed_services/modules/thumbs/selfed-cliff.ririmasse@gmail.com-377VRHZUYZFE3I9SR36AO8SBRVP6KWGDTDZ.jpg','Archimedes Law','Contoh penerapan hukum Archimedes','',0,'2018-05-21 08:22:46');
INSERT INTO `modules` VALUES (5,1,0,'http://selfed.co/selfed_services/modules/selfed-cliff.ririmasse@gmail.com-LNNYPMTMFYNXGBIV6GXCU085R8G627NEL03.mp4','http://selfed.co/selfed_services/modules/thumbs/selfed-cliff.ririmasse@gmail.com-LNNYPMTMFYNXGBIV6GXCU085R8G627NEL03.jpg','Child Tutor','Child Tutor example','',0,'2018-05-21 10:09:47');
INSERT INTO `modules` VALUES (6,1,0,'selfed-cliff.ririmasse@gmail.com-XOZWMGJS35JOD7M8CCOCCMWJPZ6VWG6KUW6.mp4','selfed-cliff.ririmasse@gmail.com-XOZWMGJS35JOD7M8CCOCCMWJPZ6VWG6KUW6.jpg','Tikus kecemplung','Tikus kecemplung tong .','',0,'2018-05-21 10:17:38');
INSERT INTO `modules` VALUES (7,1,0,'selfed-cliff.ririmasse@gmail.com-BRCXCVW52RV3ASDJU93LJW3JYL63Q8PSQRF.mp4','selfed-cliff.ririmasse@gmail.com-BRCXCVW52RV3ASDJU93LJW3JYL63Q8PSQRF.jpg','Rat got trapped','Video amatir','',0,'2018-05-28 04:33:11');
INSERT INTO `modules` VALUES (8,1,5,'selfed-cliff.ririmasse@gmail.com-F5D1V8J4VZD1SSSJMKNDKZPZ9DPD7QQDLT4.mp4','selfed-cliff.ririmasse@gmail.com-F5D1V8J4VZD1SSSJMKNDKZPZ9DPD7QQDLT4.jpg','Test visual','Teesting','',0,'2018-05-28 04:47:15');
INSERT INTO `modules` VALUES (9,1,5,'selfed-cliff.ririmasse@gmail.com-1XHBHZPAG7R2QP8CVPNC8M1MH4FLXHGO4NQ.mp4','selfed-cliff.ririmasse@gmail.com-1XHBHZPAG7R2QP8CVPNC8M1MH4FLXHGO4NQ.jpg','Testing','Testing','',0,'2018-05-28 05:05:47');
INSERT INTO `modules` VALUES (10,1,5,'selfed-cliff.ririmasse@gmail.com-TRVT5V6N34GKFXSCUWWWTVVINKAKJIB2ZWM.mp4','selfed-cliff.ririmasse@gmail.com-TRVT5V6N34GKFXSCUWWWTVVINKAKJIB2ZWM.jpg','Testing','Testing','',0,'2018-06-04 10:39:28');
INSERT INTO `modules` VALUES (11,3,4,'selfed-Liss4444@yahoo.com-LUZRITOXN4QOSU48K819WZ0SPDMW3QJEB8V.mp4','selfed-Liss4444@yahoo.com-LUZRITOXN4QOSU48K819WZ0SPDMW3QJEB8V.jpg','Hgjjj','Ghvggb','',0,'2018-06-04 10:45:32');
INSERT INTO `modules` VALUES (47,0,0,'UDINUS6.jpg','','aaa','aaa','Not Paid',1,'2018-12-03 16:21:35');
INSERT INTO `modules` VALUES (50,0,0,'UDINUS7.jpg','','test','test','Not Paid',1,'2018-12-10 15:58:55');
INSERT INTO `modules` VALUES (56,0,0,'hospital-icon.png','','bb','sss','Not Paid',1,'2018-12-18 09:29:36');
INSERT INTO `modules` VALUES (58,0,0,'ekosistem.mp4','','bbb','bbb','Not Paid',1,'2018-12-18 12:15:31');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table news
#

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(124) NOT NULL,
  `news_content` text NOT NULL,
  `news_author` int(11) NOT NULL,
  `news_status` int(1) NOT NULL,
  `news_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table news
#

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table quiz_answers
#

DROP TABLE IF EXISTS `quiz_answers`;
CREATE TABLE `quiz_answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_value` text NOT NULL,
  `member_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table quiz_answers
#

LOCK TABLES `quiz_answers` WRITE;
/*!40000 ALTER TABLE `quiz_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz_answers` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table quiz_keys
#

DROP TABLE IF EXISTS `quiz_keys`;
CREATE TABLE `quiz_keys` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `quiz_answer` text NOT NULL,
  `educator_id` int(11) NOT NULL,
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table quiz_keys
#

LOCK TABLES `quiz_keys` WRITE;
/*!40000 ALTER TABLE `quiz_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz_keys` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table quiz_questions
#

DROP TABLE IF EXISTS `quiz_questions`;
CREATE TABLE `quiz_questions` (
  `quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_group` int(11) NOT NULL,
  `quize_title` varchar(224) NOT NULL,
  `quiz_educator` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quiz_option_1` varchar(75) NOT NULL,
  `quiz_option_2` varchar(75) NOT NULL,
  `quiz_option_3` varchar(75) NOT NULL,
  `quiz_option_4` varchar(75) NOT NULL,
  PRIMARY KEY (`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table quiz_questions
#

LOCK TABLES `quiz_questions` WRITE;
/*!40000 ALTER TABLE `quiz_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz_questions` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table users
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(115) NOT NULL,
  `password` varchar(75) NOT NULL,
  `role` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Dumping data for table users
#

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'admin123','d033e22ae348aeb5660fc2140aec35850c4da997',1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
