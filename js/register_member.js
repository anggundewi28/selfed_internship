$("#submit").click(function(){
	var name = $("#name").val();
	var email = $("#email").val();
	var password = $("#password").val();
	var retype_password = $("#retype_password").val();
	var phone = $("#phone").val();
	
	if(name === "" || email === "" || password === "" || phone === ""){
		alert("Please Complete Your Form");
	}
	else{
		if(retype_password == password){
			$.ajax({
				type : "POST",
				url : assign_url,
				dataType: "JSON",
				data:{
					flag: "register",
					name : name,
					email : email,
					password : password,
					phone : phone
				},
				success: function(){
					alert("Success");
				},
				error: function(){
					alert("Connection Error ");
				}
			});
		}
		else{
			alert("Your password is not the same");
		}
	}
});