<!DOCTYPE html>
<html>
    <head>
		<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
		<title>Cofounder Network</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css" />
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/form-style.css" />
		<link rel="stylesheet" type="text/css" href="css/color-style.css" />
		<link rel="stylesheet" type="text/css" href="css/box-style.css" />
		<link rel="stylesheet" type="text/css" href="css/animation.css" />
		
	</head>
	<body>
		<!-- Loading -->
		<div id="app_loader" class="center_box" >
			<div class="center_frame" >
				<div style="width: 128px; height: 128px; position: relative; margin: auto;">
					<img src="img/center_logo.png" class="center_logo" />
					<img src="img/center_loading.png" class="center_logo_loading fa-spin" />
				</div>
			</div>
		</div>
		
		<!-- Popout Message -->
		<div id="app_popout" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.55);  z-index: 89457777">
			<div class="center_frame" >
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" id="app_popout_state">
						
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		
		<!-- blockout Message -->
		<div id="app_blockout" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.55); z-index: 89457787">
			<div class="center_frame" >
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" id="app_blockout_state">
						
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		
		<button class="bn-small " id="app_main_login" style="display: none;">LOGIN <i class="fa fa-key"></i></button>
		<button class="bn-small bc-red" id="app_main_signup" style="display: none;" >SIGNUP</button>
		
		<div id="app_main_loginform" style="display: none;">
			<div id="app_main_loginform_close" style="width: 10px; height: 10px; position: absolute; top: 10px; right: 20px;">
				<i class="fa fa-chevron-up"></i>
			</div>
			<input type="text" class="form-center-white" id="app_main_loginform_email" placeholder="Username" />
			<input type="password" class="form-center-white" id="app_main_loginform_pass" placeholder="Password" />
			<button class="form-center-1-button bc-orange" id="app_main_loginform_send">LOGIN</button>
			<div style="height: 10px;"></div>
			<a href="#" id="app_main_loginform_forgot">Forgot Password ?</a>
		</div>
		
		<!-- Forgot Password -->
		<div id="app_forgot" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48);">
			<div class="center_frame">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
						<button id="app_forgot_close" class="btn btn-circle bc-red" style="position: absolute; top: -10px; right: -5px;"><i class="fa fa-times"></i></button>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<input type="text" class="form-center-white" id="app_forgot_email" placeholder="Your registerd email address" />
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<button class="form-center-1-button bc-green" id="app_forgot_send">
									Reset Password
								</button>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		
		
		<div id="app_main" class="bgimg-front" style="display: none;">
			
		
			<div class="caption">
				<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<img src="img/front001.jpg" style="width: 100%;" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<img src="img/front002.jpg" style="width: 100%;" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<img src="img/front003.jpg" style="width: 100%;" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<img src="img/front004.jpg" style="width: 100%;" />
					</div>
				</div>
				
			</div>
			</div>
		</div>
		
		<!-- SignUp -->
		<div id="app_main_reg" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.75);">
			<div class="center_frame">
				<input type="hidden" id="app_main_reg_terms_yes" />
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
						<button id="app_main_reg_close" class="btn btn-circle bc-red" style="position: absolute; top: -10px; right: -5px;"><i class="fa fa-times"></i></button>
						
						<input type="text" id="app_main_reg_username" placeholder="Username" class="form-center" />
						<div class="form-center bc-red" style="display: none;" id="app_main_reg_username_error">Username <span id="app_main_reg_username_er_name"></span> already taken, please use another username</div>
						
						<input type="text" id="app_main_reg_email" placeholder="Email Address" class="form-center" />
						<input type="text" id="app_main_reg_name" placeholder="Fullname" class="form-center" />
						
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="password" id="app_main_reg_pass" placeholder="Password" class="form-center" />
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="password" id="app_main_reg_repass" placeholder="Repeat Password" class="form-center" />
							</div>
						</div>
						
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div style="font-size: 23px; color: #888; float: left; margin-left: 10px;" id="app_main_reg_terms">
									<i class="fa fa-square-o"></i>
								</div> 
								<div style="float: left; padding-top: 4px; width: 80%;">I accept the <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></div>
								<div style="clear: both;"></div>
							</div>
						</div>
						
						<button class="form-center-1-button bc-pale-blue"  id="app_main_reg_send">
							SIGNUP <i class="fa fa-check"></i>
						</button>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		
		<!-- Logout Confirm -->
		<div id="app_logout" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48);">
			<div class="center_frame" >
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p>Do you want to signout ?<p>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<button class="form-center-1-button bc-red"  id="app_logout_no">
									No <i class="fa fa-times"></i>
								</button>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<button class="form-center-1-button bc-green"  id="app_logout_yes">
									Yes <i class="fa fa-check"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		<!-- End Confirm Sign Out -->
		
		<!-- Inside Page After Login -->
		<!-- Headmenu -->
		<div class="headbar container" id="app_main_head" style="display: none; background: #fff; box-shadow: 0px 0px 12px #3e3e3e; padding-top: 7px;">
			<div class="row" >
				<div class="col-md-1 col-sm-1 col-xs-12 headmenu">
					CoFound
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12">
				
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="row">
						<div class="col-md-3 sol-sm-3 col-xs-12 headmenu" style="border-left: 1px solid #f1f1f1; border-right: 1px solid #f1f1f1;" id="app_inside_headmenu_project" >
							PROJECTS
						</div>
						<div class="col-md-3 sol-sm-3 col-xs-12 headmenu" style="border-right: 1px solid #f1f1f1;" id="app_inside_headmenu_network" >
							NETWORKS
						</div>
						<div class="col-md-3 sol-sm-3 col-xs-12 headmenu" style="border-right: 1px solid #f1f1f1;" id="app_inside_headmenu_discuss" >
							BLOG
						</div>
						<div class="col-md-3 sol-sm-3 col-xs-12"  id="app_inside_profile" style="border-bottom: 5px solid #fff;">
							<center>
								<img src="img/thumb.jpg" style="max-width: 36px; margin-top: 7px; " class="img-responsive img-circle" />
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Headmenu -->
		
		<!-- Head Submenu -->
		<div id="app_inside_profilebar" style="display: none; ">
			<div class="profilebar_button" id="app_inside_sidemenu_profile" style="border-bottom: 1px solid #e1e1e1;">
				<i class="fa fa-user"></i> My Profile
			</div>
			<div class="profilebar_button" id="app_inside_sidemenu_message" style="border-bottom: 1px solid #e1e1e1;">
				<i class="fa fa-envelope"></i> Message
			</div>
			<div class="profilebar_button" id="app_inside_sidemenu_mynetwork" style="border-bottom: 1px solid #e1e1e1;">
				<i class="fa fa-sitemap"></i> My Network
			</div>
			<div class="profilebar_button"  style="border-bottom: 1px solid #e1e1e1;">
				<i class="fa fa-support"></i> Support
			</div>
			<div class="profilebar_button" id="app_inside_profilebar_logout">
				<i class="fa fa-lock"></i> Logout
			</div>
		</div>
		<!-- End of Head Submenu -->
		
		<div id="app_inside" style="display: none;">
			<div class="container">
				
				<!-- Discuss Page -->
				<div class="row" style="display: none; border-top: 1px solid #e1e1e1; " id="app_inside_discuss">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12"></div>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<input tyep="text" class="form-center-search" placeholder="Search Disscusstion"/>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12"></div>
						</div>
					
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12">
								<h3 class="topic_list" style="text-align: right; color: #3292cf;">Trending Discussions</h3>
								<div id="app_inside_sidetopic_list">
									<p>Crowdfunding - Kickstarter Pre-Launch</p>
									<p>Fundraising Options Available To An Early Stage Start-up Project Besides Crowdfunding</p>
									<p>What to do when your MVP will cost at least USD 350K?</p>
								</div>
							</div>
							
							<div class="col-md-7 col-sm-7 col-xs-12">
								
								<div class="discuss_list">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<h3 style="color: #2e2e2e; font-size: 24px;">Crowdfunding - Kickstarter Pre-Launch</h3></div>
										</div>
									<div class="row">
										<div class="col-md-2 col-sm-3 col-xs-3">
											<img src="img/thumb.jpg" class="img-circle" style="width: 100%;" />
										</div>
										<div class="col-md-10 col-sm-9 col-xs-9">
											<p style="color: #333; font-szie: 18px;">Saquib Khan,PMP,CSM,CSPO - www.Dup-Dup.com Even an ounce of arrogance kills humility. - Saquib Khan - www.Dup-Dup.com </p>
											<p>April 17th, 2018</p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<p>Hello World, Our project has been approved by Kickstarter. Yay!!! A digital A.I. buddy that knows what you like, gives you what you want and makes... <span style="color: #3292cf;">More</span></p>
										</div>
									</div>
								</div>
								
								<div class="discuss_list">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<h3 style="color: #2e2e2e; font-size: 24px;">Fundraising Options Available To An Early Stage Start-up Project Besides Crowdfunding</h3>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2 col-sm-3 col-xs-3">
											<img src="img/thumb.jpg" class="img-circle" style="width: 100%;" />
										</div>
										<div class="col-md-10 col-sm-9 col-xs-9">
											<p style="color: #333; font-szie: 18px;">AbdurRazaq Osuolale</p>
											<p>Last updated on April 17th, 2018 </p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<p>l'm working on a project that combines top two internet industries (social media and e-learning) to create a first of its kind. The project tosses... <span style="color: #3292cf;">More</span></p>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-md-2 col-sm-2 col-xs-12">
								<h3 class="topic_list" style="text-align: left;">Trending Topic</h3>
								<!--  -->
								<div id="app_inside_sidetopic_listright">
									<p>Entrepreneurship</p>
									<p>Startups</p>
									<p>Marketing Strategy</p>
								</div>
								
								<!-- Adv Banner -->
								<img src="img/adv_banner.jpg" style="width: 100%;" />
								<!-- End of Adv Banner -->
							</div>
						</div>
					</div>
				</div>
				<!-- End Discuss Page -->
				
				<!-- Project Page -->
				<div class="row" id="app_inside_project" style="display: none; ">
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row" style="margin-top: 80px;">
							
							<!-- User Items -->
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div style="border-radius: 8px; box-shadow: 0px 0px 3px #777;">
									<div style="width: 100%; overflow-x: auto; height: 320px;">
										<table style="width: 100%;">
											
											<tr>
												<td style="width: 60px; padding: 10px;">
													<button class="btn-circle-big bc-orange">
														C
													</button>
												</td>
												<td style="vertical-align: top;">
													<h3 style="font-size: 21px; margin-top: 10px;">Cliff Michael</h3>
													<p style="font-size: 14px;"><i class="fa fa-mars"></i></p>
												</td>
											</tr>
											
											<tr>
												<td style="vertical-align: top; width: 60px; padding: 10px;">
													Skills
												</td>
												<td style="vertical-align: top;">
													<div class="skill_holder">
														<div class="skill_item">Coding</div>
														<div class="skill_item">Marketing</div>
														<div class="skill_item">Mobile Development</div>
														<div class="skill_item">Coding</div>
														<div class="skill_item">Marketing</div>
														<div class="skill_item">Mobile Development</div>
														<div class="skill_item">Coding</div>
														<div class="skill_item">Marketing</div>
														<div class="skill_item">Mobile Development</div>
														<div class="skill_item">Coding</div>
														<div class="skill_item bc-cyan">+1more</div>
													</div>
												</td>
											</tr>
											
										</table>
									</div>
									
									<div class="row" style="width: 100%; padding: 10px 0px 10px 0px;">
										<div class="col-md-6 col-sm-6 col-xs-6" style="padding: 0px;">
											
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6" style="padding: 0px;">
											<button class="form-center-1-button bc-pale-blue">Connect</button>
										</div>
									</div>
								</div>
							</div>
							<!-- End User Items -->
							
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
				</div>
				<!-- End Network Page -->
				
				<!-- Network Page -->
				<div class="row" id="app_inside_network" style="display: none;">
					<input type="hidden" id="app_inside_network_go" />
					<input type="hidden" id="app_inside_network_filter" />
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 60px; position: relative;">
						<input type="text" id="app_inside_network_search" class="full-white-input" placeholder="Skill you desired"/>
						<div id="app_inside_network_search_result"></div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row" style="margin-top: 10px;" id="app_inside_network_list">
							
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
				</div>
				<!-- End Network Page -->
				
				<!-- Uploader Progress -->
				<div class="center_box" id="app_reg_media_uploader" style="display: none; background: rgba(0, 0, 0, 0.55);">
					<div class="center_frame">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12"></div>
							<div class="col-md-6 col-sm-6 col-xs-12" style="background: #fff; padding: 20px;">
								<p>We are uploading your file, please wait</p>
								<hr/>
								<div id="app_reg_media_uploader_progress">
									<div id="app_reg_media_uploader_fill"></div>
									<span id="app_reg_media_uploader_size"></span>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12"></div>
						</div>
					</div>
				</div>
				<!-- End Uploader Progress -->
				
				<!-- Delete Confirm -->
				<div id="app_inside_myprofile_deletedoc_confirm" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48);">
					<input type="hidden" id="app_inside_myprofile_deletedoc_confirm_id" />
					<div class="center_frame" >
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-1"></div>
							<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
								
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<p>Do you want to remove the file ?<p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-red"  id="app_inside_myprofile_deletedoc_confirm_no">
											No <i class="fa fa-times"></i>
										</button>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-green"  id="app_inside_myprofile_deletedoc_confirm_yes">
											Yes <i class="fa fa-check"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-1"></div>
						</div>
					</div>
				</div>
				<!-- End Delete Confirm -->
				
				<!-- Profile Form Editor -->
				<div id="app_inside_myprofile_bioedit" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48);">
					<input type="hidden" id="app_inside_myprofile_bioedit_gender" />
					
					<div class="center_frame">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-1"></div>
							<div class="col-md-6 col-sm-6 col-xs-10 clean-straight-box" style="position: relative; border-radius: 10px;">
								<button id="app_inside_myprofile_bioedit_close" class="btn btn-circle bc-red" style="position: absolute; top: -10px; right: -5px;"><i class="fa fa-times"></i></button>
								
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<h3>Your Profile<h3>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px; text-align: left;">
										<input type="text" class="form-center" placeholder="Your Name" id="app_inside_myprofile_bioedit_name" />
										<input type="text" class="form-center" placeholder="Phone Number" id="app_inside_myprofile_bioedit_number" />
										<div class="btn-group" style="margin-top: 10px;">
											<button id="app_inside_myprofile_bioedit_male" class="btn btn-primary">Male</button>
											<button id="app_inside_myprofile_bioedit_female" class="btn btn-default">Female</button>
										</div>
										<textarea class="form-center" placeholder="Describe about your self in a few words" id="app_inside_myprofile_bioedit_desc"></textarea>
									</div>
									
									<div class="col-md-6 col-sm-6 col-xs-6">
										
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-green" style="border-radius: 5px;" id="app_inside_myprofile_bioedit_save">
											Save <i class="fa fa-check"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-1"></div>
						</div>
					</div>
				</div>
				<!-- End Profile Form Editor -->
				
				<!-- Connect Confirm -->
				<div id="app_inside_myprofile_connect_confirm" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48);">
					<input type="hidden" id="app_inside_myprofile_connect_confirm_id" />
					<div class="center_frame" >
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-1"></div>
							<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
								
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<p>Do you want to connect with <br/><span style="font-size: 18px;" id="app_inside_myprofile_connect_confirm_name"></span> ?<p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-red"  id="app_inside_myprofile_connect_confirm_no">
											No <i class="fa fa-times"></i>
										</button>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-green"  id="app_inside_myprofile_connect_confirm_yes">
											Yes <i class="fa fa-check"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-1"></div>
						</div>
					</div>
				</div>
				<!-- End Connect Confirm -->
				
				<!-- Confirm Request -->
				<div id="app_inside_myprofile_request_confirm" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48);">
					<input type="hidden" id="app_inside_myprofile_request_confirm_id" />
					<div class="center_frame">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-1"></div>
							<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
								
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<p>Do you want to confirm connecttion with <br/><span style="font-size: 18px;" id="app_inside_myprofile_request_confirm_name"></span> ?<p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-red"  id="app_inside_myprofile_request_confirm_no">
											No <i class="fa fa-times"></i>
										</button>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button class="form-center-1-button bc-green"  id="app_inside_myprofile_request_confirm_yes">
											Yes <i class="fa fa-check"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-1"></div>
						</div>
					</div>
				</div>
				<!-- End Confirm Request -->
				
				<!-- Profile Page -->
				<div class="row" style="display: none; border-top: 1px solid #e1e1e1; " id="app_inside_myprofile">
					<div id="app_inside_myprofile_back">
					
					</div>
					
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
					<div class="col-md-10 col-sm-10 col-xs-12" style="z-index: 100; margin-top: 180px; background: #fff;">
						<div class="row" >
							<div class="col-md-2 col-sm-2 col-xs-4" style="padding: 0px; position: relative; display: block; ">
								
								<label style="position: absolute; right: 1000px; bottom: 1000px;" class="btn btn-circle bc-pale-blue">
									 <i class="fa fa-pencil" ></i> <input type="file" hidden id="app_inside_myprofile_selfpic" />
								</label>
								
								<center id="app_inside_myprofile_selfthumb">
									<img src="img/thumb.jpg" style="width: 100%;"/>
								</center>
								
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8" style="position: relative;">
								<button style="position: absolute; right: 10px; top: 10px;" id="app_inside_myprofile_editprofile" class="btn btn-circle bc-pale-blue"><i class="fa fa-pencil"></i></button>
								<h3 id="app_inside_myprofile_name"><i class="fa fa-refresh fa-spin"></i></h3>
								<p id="app_inside_myprofile_desc"><i class="fa fa-refresh fa-spin"></i></p>
								<p id="app_inside_myprofile_phone"><i class="fa fa-refresh fa-spin"></i></p>
								<p id="app_inside_myprofile_gender"></p>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12" id="app_inside_myprofile_list_skill_frame" style="position: relative; border-radius: 5px; box-shadow: 0px 0px 3px #999; padding: 10px; margin-top: 10px; margin-bottom: 10px;">
								<button style="position: absolute; right: 10px; top: 10px;" id="app_inside_myprofile_plus_skill" class="btn btn-circle bc-pale-blue"><i class="fa fa-pencil"></i></button>
								<h4>Skill List</h4>
								<div class="skill_holder" id="app_inside_myprofile_list_skill">
									
								</div>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12" id="app_inside_myprofile_add_skill_frame" style="position: relative; border-radius: 5px; box-shadow: 0px 0px 3px #999; padding: 10px; margin-top: 10px; margin-bottom: 10px; display: none;">
								<button style="position: absolute; right: 10px; top: 10px;" id="app_inside_myprofile_close_skill" class="btn btn-circle bc-green"><i class="fa fa-floppy-o"></i></button>
								<h4>Edit Skill</h4>
								<div class="skill_holder" id="app_inside_myprofile_add_skill_temp_contain">
								
								</div>
								<div style="clear: both;"></div>
								<hr/>
								
								<input tyep="text" style="width: 99%;" class="skill_item" id="app_inside_myprofile_add_skill_filter" placeholder="Type in your skill here" />
								
								<div style="clear: both;"></div>
								<hr/>
								
								<div class="skill_holder" id="app_inside_myprofile_add_skill">
									
								</div>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12" style="position: relative; border-radius: 5px; box-shadow: 0px 0px 3px #999; padding: 10px; margin-top: 10px; margin-bottom: 10px;">
								<label style="position: absolute; right: 10px; top: 10px; padding: 8px;" class="btn btn-circle bc-pale-blue">
									<i class="fa fa-plus"></i> <input type="file" hidden id="app_inside_myprofile_fileupload" />
								</label>
								
								<h4>Certificate / Document (allowed file : jpg, jpeg, PDF, Doc)</h4>
								<div clas="row" style="margin-top: 30px;" id="app_inside_myprofile_doclist">
									
								</div>
							</div>
							
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
				</div>
				<!-- End Profile Page -->
				
				<!-- Network Profile Page -->
				<div class="row" style="display: none; border-top: 1px solid #e1e1e1; " id="app_inside_networkprofile">
					<div id="app_inside_networkprofile_back">
					
					</div>
					
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
					<div class="col-md-10 col-sm-10 col-xs-12" style="z-index: 100; margin-top: 180px; background: #fff;">
						<div class="row" >
							<div class="col-md-2 col-sm-2 col-xs-4" style="padding: 0px; position: relative; display: block; ">
								
								<center id="app_inside_networkprofile_selfthumb">
									<img src="img/thumb.jpg" style="width: 100%;"/>
								</center>
								
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8" style="position: relative;">
								
								<h3 id="app_inside_networkprofile_name"><i class="fa fa-refresh fa-spin"></i></h3>
								<p id="app_inside_networkprofile_desc"><i class="fa fa-refresh fa-spin"></i></p>
								<p id="app_inside_networkprofile_phone"><i class="fa fa-refresh fa-spin"></i></p>
								<p id="app_inside_networkprofile_gender"></p>
							
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12" id="app_inside_myprofile_list_skill_frame" style="position: relative; border-radius: 5px; box-shadow: 0px 0px 3px #999; padding: 10px; margin-top: 10px; margin-bottom: 10px;">
								<h4>Skill List</h4>
								<div class="skill_holder" id="app_inside_networkprofile_list_skill">
									
								</div>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12" style="position: relative; border-radius: 5px; box-shadow: 0px 0px 3px #999; padding: 10px; margin-top: 10px; margin-bottom: 10px;">
								
								<h4>Certificate / Document</h4>
								<div clas="row" style="margin-top: 30px;" id="app_inside_networkprofile_doclist">
									
								</div>
							</div>
							
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-12"></div>
				</div>
				<!-- End Network Profile Page -->
				
				<!-- My Network Page -->
				<div class="row" style="display: none; border-top: 1px solid #e1e1e1;" id="app_inside_mynetwork">
					<div class="col-md-12 col-sm-12 col-xs-12"></div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row" style="margin-top: 70px;" id="app_inside_mynetwork_list">
							
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12"></div>
				</div>
				<!-- End My Network Page -->
				
				<!-- Form New Message -->
				<div id="app_inside_message_popout" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.48); z-index: 8700000;">
					<input type="hidden" id="app_inside_message_popout_userid" />
					<input type="hidden" id="app_inside_message_popout_filepath" />
					<div class="center_frame">
						<div class="row">
							<div class="col-md-3 col-sm-2 col-xs-1"></div>
							<div class="col-md-6 col-sm-8 col-xs-10 clean-straight-box" style="position: relative; ">
								<button id="app_inside_message_popout_close" class="btn btn-circle bc-red" style="position: absolute; top: -10px; right: -5px;">
									<i class="fa fa-times"></i>
								</button>
								
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<textarea class="form-center-area-white" id="app_inside_message_popout_message" placeholder="Write your message"></textarea>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12"></div>
									<div class="col-md-3 col-sm-3 col-xs-12">
										<div class="row">
											<div class="col-md-6 col-sm-6 col-xs-6" style="padding: 1px;">
												<label class="form-center-1-button bc-cyan2">
													 <i class="fa fa-paperclip" ></i> <input id="app_inside_message_popout_file" type="file" hidden />
												</label>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6" style="padding: 1px;">
												<button class="form-center-1-button bc-cyan2" id="app_inside_message_popout_send">
													SEND <i class="fa fa-paper-plane-o"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="col-md-1 col-sm-1 col-xs-12"></div>
								</div>
								
							</div>
							<div class="col-md-3 col-sm-2 col-xs-1"></div>
						</div>
					</div>
				</div>
				<!-- End Form New Message -->
				
				<!-- Messages -->
				<div class="row" style="display: none; margin-top: 50px;" id="app_inside_message">
					<div class="col-md-4 col-sm-4 col-xs-12" style="border-right: 1px solid #e1e1e1; background: #fbfbfb;">
						
						<div class="row" style="padding-top: 10px; padding-bottom: 10px; border-bottom: 1px solid #e1e1e1;">
							<div class="col-md-10 col-sm-10 col-xs-10" style="padding-top: 10px;">
								<p style="font-size: 18px;">My Network</p>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-2" style="padding-top: 10px;">
								<i class="fa fa-search"></i>
							</div>
						</div>
						
						<div id="app_inside_message_userlist">
							
						</div>
						
						<div class="row" style="padding-top: 10px; padding-bottom: 10px; border-bottom: 1px solid #e1e1e1;">
							<div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px;">
								<p style="font-size: 18px;">Load more</p>
							</div>
						</div>
						
					</div>
					
					<div class="col-md-8 col-sm-8 col-xs-12"  style="background: #fff;">
						
						<div class="row" style="padding-top: 10px; padding-bottom: 10px; border-bottom: 1px solid #e4e4e4;">
							<div class="col-md-9 col-sm-9 col-xs-8" style="padding-top: 10px;">
								<p style="font-size: 18px;" id="app_inside_message_headname"></p>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-2" style="padding-top: 10px;">
							
							</div>
							<div class="col-md-1 col-sm-1 col-xs-2" style="position: relative; padding-top: 10px;" id="app_inside_message_new">
								<p style="font-size: 18px; text-align: right;"><i class="fa fa-envelope-o"></i></p>
								<p style="font-size: 10px; text-align: right; position: absolute; top: 5px; right: 7px;"><i class="fa fa-plus"></i></p>
							</div>
						</div>
						
						<div id="app_inside_message_detail" style="width: 100%; padding: 0px; background: #fff; overflow-y: auto; overflow-x: hidden;">
							
						</div>
					</div>
				</div>
				<!-- End Messages -->
				
			</div>
		</div>
		<!-- End of Inside Page -->
		
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/numeric.js"></script>
		<script type="text/javascript" src="js/jammer.js"></script>
		<script type="text/javascript" src="js/resetter.js"></script>
		
		<script type="text/javascript" src="script/app_loader.js"></script>
		<script type="text/javascript" src="script/app_main_reg.js"></script>
		<script type="text/javascript" src="script/app_main_loginform.js"></script>
		<script type="text/javascript" src="script/app_inside.js"></script>
		<script type="text/javascript" src="script/app_logout.js"></script>
		<script type="text/javascript" src="script/app_inside_sidetopic_list.js"></script>
		<script type="text/javascript" src="script/app_inside_myprofile.js"></script>
		<script type="text/javascript" src="script/app_inside_myprofile_bioedit.js"></script>
		<script type="text/javascript" src="script/app_forgot.js"></script>
		<script type="text/javascript" src="script/app_main_head.js"></script>
		<script type="text/javascript" src="script/app_inside_profilebar.js"></script>
		
		<!-- Network -->
		<script type="text/javascript" src="script/app_inside_network.js"></script>
		<script type="text/javascript" src="script/app_inside_myprofile_connect_confirm.js"></script>
		<script type="text/javascript" src="script/app_inside_mynetwork.js"></script>
		
		<!-- Messages -->
		<script type="text/javascript" src="script/app_inside_message.js"></script>
		<script type="text/javascript" src="script/app_inside_message_popout.js"></script>
		
		<script>
			var assign_url = "http://<?php echo $_SERVER['SERVER_NAME'];?>/cofounder/index.php/services/";
			
			jQuery(window).load(function(){
				 setTimeout(function(){
					  idle();
				 }, 3000);
			});
		</script>
		
		<script type="text/javascript"> 
			$(document).ready(function() { var ctrlDown = false; var ctrlKey = 17, vKey = 86, cKey = 67,xKey = 88, aKey = 65, LarrowKey = 37, RarrowKey = 39, ShiftKey = 16; $(document).keydown(function(e) { if (e.keyCode == ctrlKey || e.keyCode == ShiftKey) ctrlDown = true; }).keyup(function(e) { if (e.keyCode == ctrlKey || e.keyCode == ShiftKey) ctrlDown = false; }); $(".no-copy-paste").keydown(function(e) { if (ctrlDown && (e.keyCode == vKey || e.keyCode == cKey || e.keyCode == xKey || e.keyCode == aKey || e.keyCode == LarrowKey || e.keyCode == RarrowKey)) return false; }); }); 
		</script>
	</body>
</html>