<div class="content" id="main">
    <div class="container-fluid">
	<a href="<?php echo base_url(); ?>members/form"><button type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-plus"></span> Add New Member</button></a>
        <div class="row">
			<table >
			<td width="20"><td>
			<td>
			<?php echo form_open("members/search"); ?>
				<div class="col-sd-11">
					<input type="text" name="keyword" placeholder="search" class="form-control"/>  
					<input type="submit" name="submit_search" value="search" class="btn btn-success"> 
				</div>
			<?php echo form_close(); ?>
			</td>
			</table>
			<br>
        	</br>
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Name</th>
                            	<th>Email</th>
                            	<th>Phone</th>
                            	<th>Registered</th>
                            	<th>Status</th>
                            	<th>Action</th>
                            </thead>
                            <tbody>

                                <?php if (is_numeric($this->uri->segment('3'))) {
                                            $no = $this->uri->segment('3') + 1 ;
                                        } else {
                                            $no = 1;
                                    }  foreach($members as $member) { ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $member->name; ?></td>
                                	<td><?php echo $member->email; ?></td>
                                	<td><?php echo $member->phone; ?></td>
                                	<td><?php echo $member->regdate; ?></td> 
                                	
                                	<td><?php if( $member->status == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>
									<td>
                                	<button class="btn btn-success member_edit" type="submit">
										Edit
										<input type="hidden" class="app_member_edit_id" value="<?php echo $member->id; ?>" />
										<input type="hidden" class="app_member_edit_name" value="<?php echo $member->name; ?>" />
										<input type="hidden" class="app_member_edit_email" value="<?php echo $member->email; ?>" />
										<input type="hidden" class="app_member_edit_phone" value="<?php echo $member->phone; ?>" />
									</button>
                                        <a href="<?php echo base_url().'members/delMember/'.$member->id; ?>"><button class="btn btn-danger"> Delete </button> </a> 
									</td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                        
                        <?php echo $this->pagination->create_links(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content" id="edit_member" style="display: none;">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12">
    			<div class="card" style="padding: 10px;">
                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Name </div>
					    <div class="col-sd-11"> <input type="text" id="name" name="name" placeholder="Ad's Name or Title" class="form-control" /> </div>
					</div> 

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Email </div>
					    <div class="col-sd-11"> <input type="text" id="email" name="email" placeholder="Ad's Email" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Password </div>
					    <div class="col-sd-11"> <input id="password" type="password" name="password" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Phone </div>
					    <div class="col-sd-11"> <input type="text" id="phone" name="phone" placeholder="Ad's Phone" class="form-control" /> </div>
					</div>

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="submit" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
					
                    <button id="edit_member_save" type="submit" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Edit</button>
					<a href="<?php echo base_url(); ?>members/index"><button id="batal" type="button" class="btn btn-danger btn-fill" style="margin-bottom: 15px;"><span class="ban ti-close"></span> Batal</button></a>
				</div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>/js/jquery.js"></script>
<script type="text/javascript">
	var assign_url = "<?php echo base_url(); ?>index.php/Services/";

	function getValue(id) {
		var assign_url = "<?php echo base_url(); ?>Services/";

		$.ajax({
			type: "POST",
			url: assign_url,
			dataType : 'json',
			data: {
				flag : "ban_this_user",
				user_id : id,
				value : 0,
			},
			success: function () {
				alert('User banned!');
				location.reload();

			},error: function(){
				alert('error has occurred!');
			}
		});
	}
	
	$("#btn_edit").click(function(){
		$("#edit_member").fadeIn();
		$("#main").hide();
		
		$("#submit").show();
		$("#edit_member_save").hide();
	});
	
	$(".member_edit").click(function(){
		var id = $(this).children('.app_member_edit_id').val();
		var name = $(this).children('.app_member_edit_name').val();
		var email = $(this).children('.app_member_edit_email').val();
		var phone = $(this).children('.app_member_edit_phone').val();
		
		$("#id").val(id);
		$("#name").val(name);
		$("#email").val(email);
		$("#phone").val(phone);
		
		$("#edit_member").fadeIn();
		$("#main").hide();
		
		$("#submit").hide();
		$("#edit_member_save").show();
	});
	
	$("#edit_member_save").click(function(){
		var id = $("#id").val();
		var name = $("#name").val();
		var email = $("#email").val();
		var password = $("#password").val();
		var phone = $("#phone").val();
		
		if(name === "" || email === "" || password === "" || phone === ""){
			alert("Please Complete Your Form");
		}
		else{
			$.ajax({
				type : "POST",
				url : assign_url,
				dataType: "JSON",
				data:{
					flag: "edit_user_member",
					id : id,
					name : name,
					email : email,
					password : password,
					phone : phone
				},
				success: function(){
					alert("Success");
				},
				error: function(){
					alert("Connection Error ");
				}
			});
		}
	});

</script>