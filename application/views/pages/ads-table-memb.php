<div class="content">
    <div class="container-fluid">
        <a href="<?php echo base_url(); ?>ads_memb/form"><button type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-plus"></span> Add New Ad</button></a>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Name</th>
                            	<th>Preview</th>
                            	<th>Status</th>
                            	<th>Registered</th>
                            	<th>Action</th>
                            </thead>
                            <tbody>

                            <?php 
                                if (is_numeric($this->uri->segment('3'))) {
                                            $no = $this->uri->segment('3') + 1 ;
                                        } else {
                                            $no = 1;
                                } 

                                foreach($ads as $ad) { ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $ad->ad_name; ?></td>
                                	<td><a href="<?php echo $ad->ad_url; ?>"><img height="50px" src="<?php echo $ad->ad_path; ?>"></a></td>
                                	                               	
                                	<td><?php if( $ad->ad_status == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>

                                	<td><?php echo $ad->ad_timestamp; ?></td>

                                	<td> 
                                        <a href="<?php echo base_url().'ads_memb/form/'.$ad->ad_id; ?>"><button class="btn btn-success">Edit</button> </a>
                                        <a href="<?php echo base_url().'ads_memb/delete_ad/'.$ad->ad_id; ?>"><button class="btn btn-danger"> Delete </button> </a> 
                                    </td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                        
                        <?php echo $this->pagination->create_links(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {
		var assign_url = "http://selfed.co/selfed_services/index.php/Services/";

		$('.ban').click(function(){
			alert($('.ban').val());
			// $.ajax({
			// 	type: "POST",
			// 	url: assign_url,
			// 	dataType : 'json',
			// 	data: {
			// 		flag : "idle",
			// 		username : localStorage.getItem("selfed_sesseion")
			// 	},
			// 	success: function (idle) {
					
			// 	},error: function(){
					
			// 	}
			// });
		});
	});
</script>