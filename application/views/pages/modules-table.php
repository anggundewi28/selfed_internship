
<div class="content" id="table">
    <div class="container-fluid">
    <a href="<?php echo base_url(); ?>modules/form"><button type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-plus"></span> Add New Modules</button></a>
        <div class="row">
        
        <table >
        <td width="20"><td>
        <td>
        <?php echo form_open("modules/search"); ?>
            <div class="col-sd-11">
                <input type="text" name="keyword" placeholder="search" class="form-control"/>  
				<input type="submit" name="submit_search" value="search" class="btn btn-success"> 
            </div>
        <?php echo form_close(); ?>
        </td>
        </table>
        <br>
        </br>   
        <?php echo "<h5>".$this->session->flashdata('alert')."</h5>"; ?>
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Title</th> 
                            	<th>Description</th>
                                <th>Payment</th>
                            	<th>File</th>
                                <th>Status</th>
                            	<th>Upload Date</th>
                            	<th>Action</th>
                            </thead>
                            <tbody>
                            
                                <?php 
                                        if (is_numeric($this->uri->segment('3'))) {
                                            $no = $this->uri->segment('3') + 1 ;
                                        } else {
                                            $no = 1;
                                        } 

                                        foreach($modules as $module) { ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $module->title; ?></td> 
                                	<td><?php echo $module->description; ?></td>
                                    <td><?php echo $module->pembayaran; ?></td>
                                	<td><video width="200" height="200" > 
											<source src="<?php echo base_url("modulesview/".$module->path) ?>" type="video/mp4"/>
										</video>
										<img width="100" height="100" src="<?php echo base_url("modulesview/".$module->path) ?>"/>
									</td>
                                	<td><?php if( $module->status == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>

                                    <td><?php echo  $module->stamp; ?></td>

                                	<td> 
                                        <a href="<?php echo base_url().'modules/formedit/'.$module->id; ?>"><button type="button" class="btn btn-success">Edit</button> </a>
                                        <a href="<?php echo base_url().'modules/delModule/'.$module->id; ?>"><button class="btn btn-danger"> Delete </button> </a> 
                                    </td>
                                </tr>

                                <?php   } ?>

                            </tbody>
                        </table>
                         <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {
		var assign_url = "http://selfed.co/selfed_services/index.php/Services/";

		$('.ban').click(function(){
			alert($('.ban').val());
			// $.ajax({
			// 	type: "POST",
			// 	url: assign_url,
			// 	dataType : 'json',
			// 	data: {
			// 		flag : "idle",
			// 		username : localStorage.getItem("selfed_sesseion")
			// 	},
			// 	success: function (idle) {
					
			// 	},error: function(){
					
			// 	}
			// });
		});
	});
</script>