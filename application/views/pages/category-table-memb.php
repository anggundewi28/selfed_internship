<div class="content">
    <div class="container-fluid">
		<a href="<?php echo base_url(); ?>category_memb/form"><button type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-plus"></span> Add New Category</button></a>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">

                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Name Category</th>
                            	<th>Status</th>
								<th>Action</th>
                            </thead>
                            <tbody>

                                <?php 
                                  
                                    if (is_numeric($this->uri->segment('3'))) {
                                            $no = $this->uri->segment('3') + 1 ;
                                        } else {
                                            $no = 1;
                                    } 

                                    foreach($category as $ca) { ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $ca->name; ?></td>
                                	                                	                               	
                                	<td><?php if( $ca->status == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>

                                	<td> 
                                       <a href="<?php echo base_url().'category_memb/formedit/'.$ca->id; ?>"><button type="button" class="btn btn-success">Edit</button> </a>
                                        <a href="<?php echo base_url().'category_memb/delCategory/'.$ca->id; ?>"><button class="btn btn-danger"> Delete </button> </a> 
                                    </td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                        
                        <?php echo $this->pagination->create_links(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {
		var assign_url = "http://selfed.co/selfed_services/index.php/Services/";

		<?php// if (!empty($this->session->flashdata('delete-success'))) {  ?>
            
            color = Math.floor((Math.random() * 4) + 1);

            $.notify({
                icon: "ti-info",
                message: "Article Successfuly Deleted."

            },{
                type: type[color],
                timer: 4000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });

        <?php //} ?>
	});
</script>