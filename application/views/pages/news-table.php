<div class="content">
    <div class="container-fluid">
        <a href="<?php echo base_url(); ?>news/form"><button type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-plus"></span> Add New News</button></a>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Title</th>
                            	<th>Status</th>
                            	<th>Published</th>
                            	<th>Action</th>
                            </thead>
                            <tbody>

                                <?php $i=1; foreach($news as $new) { ?> 
                                
                                <tr>
                                	<td><?php echo $i; ?> </td>
                                	<td><?php echo $new->news_title; ?></td>
                                	                               	
                                	<td><?php if( $new->news_status == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>

                                	<td><?php echo $new->news_timestamp; ?></td>

                                	<td> 
                                        <a href="<?php echo base_url().'news/form/'.$new->news_id; ?>"><button class="btn btn-success">Edit</button> </a>
                                        <a href="<?php echo base_url().'news/delete_news/'.$new->news_id; ?>"><button class="btn btn-danger"> Delete </button> </a>
                                    </td>
                                </tr>

                                <?php $i++; } ?>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {
		var assign_url = "http://selfed.co/selfed_services/index.php/Services/";

        <?php if (!empty($this->session->flashdata('delete-success'))) {  ?>
            
            color = Math.floor((Math.random() * 4) + 1);

            $.notify({
                icon: "ti-info",
                message: "News Successfuly Deleted."

            },{
                type: type[color],
                timer: 4000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });

        <?php } ?>
		
	});
</script>