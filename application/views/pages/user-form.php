<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12">
    			<div class="card" style="padding: 10px;">
                    <?php //echo form_open_multipart(base_url().'certificates/add', array('id'=>'form-ads', 'data-parsley-validate'=>'data-parsley-validate','class'=>'form-horizontal form-label-left', 'name'=>'form_ads')) ?>

                    <form>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Username</div>
					    <div class="col-sd-11"> <input id="username" type="text" name="username" placeholder="Username here" class="form-control" /> </div>
					</div>
                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Password</div>
					    <div class="col-sd-11"> <input id="password" type="password" name="password" class="form-control" /> </div>
					</div>

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
					<a href="<?php echo base_url(); ?>users/index"><button id="batal" type="button" class="btn btn-danger btn-fill" style="margin-bottom: 15px;"><span class="ban ti-close"></span> Batal</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {
		var assign_url = "<?php echo base_url(); ?>index.php/Services/";

		<?php 

			if (is_numeric($this->uri->segment(3))) { ?>
				$('#username').val('<?php echo $users->username; ?>');
				$('#id').val('<?php echo $users->user_id; ?>');
				var flagy = 'update_user';
		
		<?php } else {  ?>
		
				var flagy = 'save_user';
		
		<?php } ?>


		$('#submit').click(function(){
			$.ajax({
				type: "POST",
				url: assign_url,
				dataType : 'json',
				data: {
					flag : flagy,
					username : $('#username').val(),
					password : $('#password').val(),
					id : $('#id').val(),

				},
				success: function (idle) {
					location.href="<?php echo base_url(); ?>users";

				},error: function(){
					
				}
			});
		});
	});
</script>