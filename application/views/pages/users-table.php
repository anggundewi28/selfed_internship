<div class="content">
    <div class="container-fluid">
    	 <a href="<?php echo base_url(); ?>users/form"><button type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-plus"></span> Add New Admin</button></a>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Username</th>
                            	<th>Status</th>
                            	<th>Action</th>
                            </thead>
                            <tbody>

                                <?php $i=1; foreach($users as $user) { ?> 
                                
                                <tr>
                                	<td><?php echo $i; ?> </td>
                                	<td><?php echo $user->username; ?></td>
                                	
                                	<td><?php if( $user->status == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>

                                	<td> 
                                		<a href="<?php echo base_url().'users/form/'.$user->user_id; ?>"><button class="btn btn-success">Edit</button> </a>
                                        <a href="<?php echo base_url().'users/delete_user/'.$user->user_id; ?>"><button class="btn btn-danger"> Delete </button> </a> 
                                    </td>
                                </tr>

                                <?php $i++; } ?>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {

	});

	function getValue(id) {
		var assign_url = "<?php echo base_url(); ?>Services/";

		$.ajax({
			type: "POST",
			url: assign_url,
			dataType : 'json',
			data: {
				flag : "ban_this_user",
				user_id : id,
				value : 0,
			},
			success: function () {
				alert('User banned!');
				location.reload();

			},error: function(){
				alert('error has occurred!');
			}
		});
	}

</script>