<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-user"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Member</p>
                                    <?php echo $countmember; ?>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i><a href="http://localhost/selfed_services/members"> Updated now </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-pencil-alt2"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                <p>Module</p>
                                <?php echo $countmodule; ?>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-calendar"></i><a href="http://localhost/selfed_services/modules/?/modules"> Last day </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-notepad"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                <p>Articles</p>
                                <?php echo $countarticle; ?>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-timer"></i><a href="http://localhost/selfed_services/articles"> In the last hour </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-rocket"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                <p>ADS</p>
                                    <?php echo $countads; ?>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i><a href="http://localhost/selfed_services/ads"> Updated now </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Newest Members</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Name</th>
                            	<th>Email</th>
                            	<th>Phone</th>
                            	<th>Registered</th>
                            	<th>Status</th>
                            </thead>
                            <tbody>

                                <?php 
                                $no = 1;
                                foreach ($datas as $member) {
                                ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $member['name'] ?></td>
                                	<td><?php echo $member['email'] ?></td>
                                	<td><?php echo $member['phone'] ?></td>
                                	<td><?php echo $member['regdate'] ?></td>
                                	<td><?php if( $member['status'] == 1 ) { echo "Active"; } else { echo "Non Active"; }  ?></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Newest Modules</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Title</th>
                            	<th>Description</th>
                                <th>File</th>
                                <th>Upload Date</th>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($datasa as $modules) {
                                ?> 
                                <tr>
                                    <td><?php echo $no++; ?> </td>
                                    <td><?php echo $modules['title'] ?></td>
                                    <td><?php echo $modules['description'] ?></td>
                                    <td><a href="<?php echo $modules['path']; ?>"><img src="<?php echo $modules['thumb']; ?>" width="50px"></a></td>
                                    <td><?php echo $modules['stamp'] ?></td>

                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Newest Articles</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Title</th>
                            	<th>Status</th>
                            	<th>Registered</th>
                            </thead>
                            <tbody>

                                <?php 
                                $no = 1;
                                foreach ($datasb as $articles) {
                                ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $articles['article_title'] ?></td>
                                	<td><?php echo $articles['article_status'] ?></td>
                                	<td><?php echo $articles['article_timestamp'] ?></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Newest ADS</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                            	<th>Name</th>
                            	<th>Preview</th>
                            	<th>Status</th>
                            	<th>Registered</th>
                            </thead>
                            <tbody>

                                <?php 
                                $no = 1;
                                foreach ($datasc as $ads) {
                                ?> 
                                
                                <tr>
                                	<td><?php echo $no++; ?> </td>
                                	<td><?php echo $ads['ad_name'] ?></td>
                                	<td><a href="<?php echo $ads['ad_url']; ?>"><img height="50px" src="<?php echo $ads['ad_path']; ?>"></a></td>
                                	<td><?php echo $ads['ad_status'] ?></td>
                                    <td><?php echo $ads['ad_timestamp'] ?></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>