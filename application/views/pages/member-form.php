
<div class="content">
    <div class="container-fluid"> 
        
        <div class="row">
            <div class="col-md-12">
    			<div class="card" style="padding: 10px;">
					<form action="<?php echo base_url(). 'members/tambah_aksi'; ?>" method="post">
                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Name </div>
					    <div class="col-sd-11"> <input type="text" id="name" name="name" placeholder="Ad's Name or Title" class="form-control" /> </div>
					</div> 

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Email </div>
					    <div class="col-sd-11"> <input type="text" id="email" name="email" placeholder="Ad's Email" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Password </div>
					    <div class="col-sd-11"> <input id="password" type="password" name="password" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Retype Password </div>
					    <div class="col-sd-11"> <input id="retype_password" type="password" name="password" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Phone </div>
					    <div class="col-sd-11"> <input type="text" id="phone" name="phone" placeholder="Ad's Phone" class="form-control" onkeypress="return hanyaAngka(event)" /> </div>
						<script>
							function hanyaAngka(evt) {
							var charCode = (evt.which) ? evt.which : event.keyCode
							if (charCode > 31 && (charCode < 48 || charCode > 57))
					
								return false;
							return true;
							}
						</script>
					</div>

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="submit" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
					<a href="<?php echo base_url(); ?>members/index"><button id="batal" type="submit" class="btn btn-danger btn-fill" style="margin-bottom: 15px;"><span class="ban ti-close"></span> Batal</button></a>
                </div>
            </div>
        </div>
    </div>
</div>