<div class="content">
    <div class="container-fluid">
        
        <div class="row">
			<?php echo "<h3>".$this->session->flashdata('alert')."</h3>"; ?>
            <div class="col-md-12"> 
    			<div class="card" style="padding: 10px;">
				<?php echo form_open_multipart(base_url().'modules/update') ?>

                    <!-- <form> -->

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Title </div>
					    <div class="col-sd-11"> <input id="title" type="text" name="title" value="<?php echo $modules->title; ?>" placeholder="Ad's Name or Title" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Description </div>
					    <div class="col-sd-11"> <input id="description" type="text" value="<?php echo $modules->description; ?>" name="description" placeholder="Ad's Description" class="form-control" /> </div>
					</div>

					<div class="form-group">
						<div class="col-sd-1" style="padding-top: 5px;"> Payment </div>
							<select id="pembayaran" name="pembayaran" class="form-control">
								<option selected=""  value="<?php echo $modules->pembayaran ;?>"><?php echo $modules->pembayaran ;?></option>
								<option value="Paid">Paid</option>
								<option value="Not Paid">Not Paid</option>
							</select>
					</div>
						
					<div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;">Upload File </div>
					    	<div class="col-sd-11"><input id="app_inside_myprofile_selfpic" class="form-control col-md-7 col-xs-12" type="file" name="modules_image">
								<?php 
								if($modules->path==''){?>
									<label>Belum Ada Gambar</label><br>
								<?php }else{ ?>
									<img src="<?php echo base_url('./modulesview/'.$modules->path)?>" alt="" name="modules_image" height="50px" id="file"><br>
								<?php }?>
                        	</div>
					</div> 

					<input type="hidden" name="id" id="id" value="<?php echo $modules->id;?>">
					<br></br>
                    <button id="submit" type="submit" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
                    
					<?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

