<div class="content">
    <div class="container-fluid">
        <div class="row">
		
            <div class="col-md-12"> 
    			<div class="card" style="padding: 10px;">
				<?php echo form_open_multipart(base_url().'modules_memb/upload', array('id'=>'form-modules', 'class'=>'form-horizontal form-label-left', 'name'=>'form_cert')) ?>

                    <!-- <form> -->

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Title </div>
					    <div class="col-sd-11"> <input id="title" type="text" name="title" placeholder="Ad's Name or Title" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Description </div>
					    <div class="col-sd-11"> <input id="description" type="text" name="description" placeholder="Ad's Description" class="form-control" /> </div>
					</div>

					<div class="form-group">
						<div class="col-sd-1" style="padding-top: 5px;"> Payment </div>
							<select id="pembayaran" name="pembayaran" class="form-control">
								<option selected="">-- Select --</option>
								<option value="Paid">Paid</option>
								<option value="Not Paid">Not Paid</option>
							</select>
					</div>

					<div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;">Upload File </div>
					    	<div class="col-sd-11"><input id="app_inside_myprofile_selfpic" class="form-control col-md-7 col-xs-12" type="file" name="modules_image">
					    		<img  id="file">
                        	</div>
					</div> 

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="submit" value="update" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
                    <a href="<?php echo base_url(); ?>modules_memb/index"><button id="batal" type="button" class="btn btn-danger btn-fill" style="margin-bottom: 15px;"><span class="ban ti-close"></span> Batal</button></a>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Uploader Progress -->
<div class="center_box" id="app_reg_media_uploader" style="display: none; background: rgba(0, 0, 0, 0.55);">
					<div class="center_frame">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12"></div>
							<div class="col-md-6 col-sm-6 col-xs-12" style="background: #fff; padding: 20px;">
								<p>We are uploading your file, please wait</p>
								<hr/>
								<div id="app_reg_media_uploader_progress">
									<div id="app_reg_media_uploader_fill"></div>
									<span id="app_reg_media_uploader_size"></span>
								</div>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12"></div>
						</div>
					</div>
				</div>
				<!-- End Uploader Progress -->


<script type="text/javascript">
	$(function () {

		<?php 

			if (is_numeric($this->uri->segment(3))) { ?>
				$('#title').val('<?php echo $ads->ad_name; ?>');
				$('#description').val('<?php echo $ads->ad_url; ?>');
				$('#id').val('<?php echo $ads->ad_id; ?>');
				$('#file').attr('src', '<?php echo str_replace("./", base_url(), $modules->path); ?>');
				$('#file').attr('width', '300');
	
		<?php } ?>

		// $('.btn').click(function(){

		// 		var assign_url = "http://selfed.co/selfed_services/index.php/Services/";

		// 		$("#app_inside_myprofile_selfpic").change(function(){
		// 		var ext = $('#app_inside_myprofile_selfpic').val().split('.').pop().toLowerCase();
				
		// 		if($.inArray(ext, ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG']) == -1) {
		// 			popout("Only a jpg or png picture format allowed !");
		// 		}
		// 		else{
					
		// 			var progress_bar_id = '#app_reg_media_uploader_progress';
		// 			var file = this.files[0];
		// 			var name = file.name;
		// 			var size = file.size;
		// 			var type = file.type;
					
		// 			if(parseInt(size) < 1000000){
		// 				$("#app_inside_profile").html('<center><img src="img/loading.svg" style="max-width: 36px;" class="img-responsive img-circle" /></center>');
		// 				$("#app_inside_myprofile_selfthumb").html('<img src="img/loading.svg" style="width: 100%;"/>');
						
		// 				$("#app_reg_media_uploader").slideDown();
						
		// 				var file_data = $("#app_inside_myprofile_selfpic").prop("files")[0];
						
		// 				var form_data = new FormData();                  
		// 				form_data.append("flag", "upload_ads")
		// 				form_data.append("file", file_data)
										
		// 				$.ajax({
		// 						url: assign_url, 
		// 						dataType: "json",  
		// 						cache: false,
		// 						contentType: false,
		// 						processData: false,
		// 						data: form_data,                      
		// 						type: "POST",
		// 						xhr: function(){
		// 							//upload Progress
		// 							var xhr = $.ajaxSettings.xhr();
		// 							if (xhr.upload) {
		// 								xhr.upload.addEventListener('progress', function(event) {
		// 									var percent = 0;
		// 									var position = event.loaded || event.position;
		// 									var total = event.total;
		// 									if (event.lengthComputable) {
		// 										percent = Math.ceil(position / total * 100);
		// 									}
		// 									//update progressbar
		// 									$(progress_bar_id +" #app_reg_media_uploader_fill").css("width", + percent +"%");
		// 									$(progress_bar_id + " #app_reg_media_uploader_size").text(percent +"%");
		// 								}, true);
		// 							}
		// 							return xhr;
		// 						},
		// 						mimeType:"multipart/form-data"
		// 				}).done(function(res){
		// 					$("#app_reg_media_uploader").fadeOut();
							
		// 					$("#app_inside_myprofile_selfthumb").html('<img src="'+res.path+'" style="width: 100%;"/>');
		// 					$("#app_inside_profile").html('<center><img src="'+res.path+'" style="max-width: 36px;" class="img-responsive img-circle" /></center>');
		// 				});
		// 			}
		// 			else{
		// 				popout("Maximum file size is 1Mb !");
		// 			}
		// 		}
		// 	});
			
		 //});
	});
</script>