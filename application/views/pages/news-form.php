<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12">
    			<div class="card" style="padding: 10px;">
                    <?php //echo form_open_multipart(base_url().'certificates/add', array('id'=>'form-ads', 'data-parsley-validate'=>'data-parsley-validate','class'=>'form-horizontal form-label-left', 'name'=>'form_ads')) ?>
                <form>
                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Title</div>
					    <div class="col-sd-11"> <input id="title" type="text" name="title" placeholder="Ad's Name or Title" class="form-control" /> </div>
					</div>
                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Content</div>
					    <div class="col-sd-11"> <textarea id="content" name="content" placeholder="Article Content Here.." class="form-control" /> </textarea></div>
					</div>

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="button" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function () {
		var assign_url = "<?php echo base_url(); ?>index.php/Services/";

		<?php 
		
			if (is_numeric($this->uri->segment(3))) { ?>
				$('#title').val('<?php echo $news->news_title; ?>');
				$('#content').val('<?php echo $news->news_content; ?>');
				$('#id').val('<?php echo $news->news_id; ?>');
				var flagy = 'update_news';
		
		<?php } else {  ?>
		
				var flagy = 'save_news';
		
		<?php } ?>

		$('#submit').click(function(){
			$.ajax({
				type: "POST",
				url: assign_url,
				dataType : 'json',
				data: {
					flag : flagy,
					title : $('#title').val(),
					content : $('#content').val(),
					id : $('#id').val(),
				},
				success: function (idle) {
					location.href="<?php echo base_url(); ?>news";

				},error: function(){
					
				}
			});
		});
	});
</script>