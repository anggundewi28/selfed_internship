<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12">
    			<div class="card" style="padding: 10px;">
					<?php foreach($membera as $member){ ?>
					<form action="<?php echo base_url(). 'crud/update'; ?>" method="post">

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Name </div>
					    <div class="col-sd-11"> <input type="text" id="name" name="name" value="<?php echo $member->name ?>" class="form-control" /> </div>
					</div> 

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Email </div>
					    <div class="col-sd-11"> <input type="text" id="email" name="email" value="<?php echo $member->email ?>" class="form-control" /> </div>
					</div>

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Password </div>
					    <div class="col-sd-11"> <input id="password" type="password" name="password" value="<?php echo $member->password ?>" class="form-control" /> </div>
					</div>


                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Phone </div>
					    <div class="col-sd-11"> <input type="text" id="phone" name="phone" value="<?php echo $member->phone ?>" class="form-control" /> </div>
					</div>

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="submit" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
                    <a href="<?php echo base_url(); ?>members/index"><button id="batal" type="button" class="btn btn-danger btn-fill" style="margin-bottom: 15px;"><span class="ban ti-close"></span> Batal</button></a>
					</form>	
					<?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
	$(function () {
		var assign_url = "<?php echo base_url(); ?>index.php/Services/";

		<?php 

			if (is_numeric($this->uri->segment(3))) { ?>
				$('#name').val('<?php echo $member->name; ?>');
				$('#email').val('<?php echo $member->email; ?>');
                $('#password').val('<?php echo $member->password; ?>');
                $('#phone').val('<?php echo $member->phone; ?>');
				$('#id').val('<?php echo $member->id; ?>');

				var flagy = 'update_user_client';
		
        <?php } else {  ?>
		
        var flagy = 'update_user_client';

<?php } ?>
});
</script>