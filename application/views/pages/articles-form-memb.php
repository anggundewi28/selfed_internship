<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12">
    			<div class="card" style="padding: 10px;">
                    <?php echo form_open_multipart(base_url().'articles_memb/add', array('id'=>'form-article', 'class'=>'form-horizontal form-label-left', 'name'=>'form_article')) ?>

                    <!-- <form> -->

                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Title</div>
					    <div class="col-sd-11"> <input id="title" type="text" name="title" placeholder="Ad's Name or Title" class="form-control" /> </div>
					</div>

					<div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Description</div>
					    <div class="col-sd-11"> <input id="description" type="text" name="description" placeholder="Ad's Description" class="form-control" /> </div>
					</div>

					<div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Upload Cover Image</div>
					    	<div class="col-sd-11"><input id="app_inside_myprofile_selfpic" class="form-control col-md-7 col-xs-12" type="file" name="article_cover">
					    		<img id="preview">
                        	</div>
					</div>
                    <div class="form-group">
                    	<div class="col-sd-1" style="padding-top: 5px;"> Content</div>
					    <div class="col-sd-11"> <!-- <div id="snow-container"></div>  -->
					    <textarea id="content" name="content" placeholder="Article Content Here.." class="form-control" /> </textarea>
					    	
					    </div>
					</div>

					<input type="hidden" name="id" id="id">

                    <button id="submit" type="submit" class="btn btn-success btn-fill" style="margin-bottom: 15px;"><span class="ban ti-save"></span> Save</button>
					<a href="<?php echo base_url(); ?>articles_memb/index"><button id="batal" type="button" class="btn btn-danger btn-fill" style="margin-bottom: 15px;"><span class="ban ti-close"></span> Batal</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/vendor/summernote/summernote.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/vendor/summernote/summernote.min.js"></script>

<script type="text/javascript">

	 
	$(function () {
		var assign_url = "<?php echo base_url(); ?>index.php/Services/";

		$('#content').summernote({
  			maxHeight: 600, 
  			width: 800, 
  			toolbar: [
					    // [groupName, [list of button]]
					    ['style', ['bold', 'italic', 'underline', 'clear']],
					    ['font', ['strikethrough', 'superscript', 'subscript']],
					    ['fontsize', ['fontsize']],
					    ['color', ['color']],
					    ['para', ['ul', 'ol', 'paragraph']],
					    ['height', ['height']]
					  ]
		});

		<?php 

			if (is_numeric($this->uri->segment(3))) { ?>
				$('#title').val('<?php echo $articles->article_title; ?>');
				$("#content").summernote('code', '<?php echo $articles->article_content; ?>');
				$('#preview').attr('src', '<?php echo str_replace("./", base_url(), $articles->article_cover); ?>');
				$('#preview').attr('width', '300');

				$('#id').val('<?php echo $articles->article_id; ?>');
		
		<?php }  ?>
		
		// $('#submit').click(function(){

		// 	$.ajax({
		// 		type: "POST",
		// 		url: assign_url,
		// 		dataType : 'json',
		// 		data: {
		// 			flag : flagy,
		// 			title : $('#title').val(),
		// 			//content : $('#content').val(),
		// 			//content : quill.getText(0, 10),
		// 			id : $('#id').val(),

		// 		},
		// 		success: function () {
		// 			location.href="<?php echo base_url(); ?>articles";

		// 		},error: function(){
					
		// 		}
		// 	});
		// });
	});
</script>