<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Selfed.co | Self Education Platform</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/css/themify-icons.css" rel="stylesheet">
    
    <!--   Core JS Files   -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>

    <style type="text/css">
            
        /*** Upload media ***/
        #app_reg_media_uploader_progress{
            width: 100%;
            height: 32px;
            background: #00ae42;
            color: #ffffff;
            text-align: left;
            position: relative;
        }

        #app_reg_media_uploader_fill{
            top:0;
            left:0;
            position:absolute;
            height: 32px;
            background-color: #007836;
            width: 0;
        }

        #app_reg_media_uploader_size{
            top:3px;
            left:50%;
            position:absolute;
            display:inline-block;
            color: #ffffff;
        }
        
    </style>
    
</head>
<body>

<div class="wrapper">
    
    <!-- sidebar -->
    <?php echo $sidebar; ?>
    <!-- sidebar -->

    <div class="main-panel">
        
        <!-- header -->
        <?php echo $header; ?>
        <!-- header -->


        <!-- container -->
        <?php echo $content; ?>
        <!-- container -->


        <!-- footer -->
        <?php echo $footer; ?>
        <!-- footer -->

    </div>
</div>


</body>

    <!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="<?php echo base_url(); ?>assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="<?php echo base_url(); ?>assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="<?php echo base_url(); ?>assets/js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	// demo.initChartist();

        	// $.notify({
         //    	icon: 'ti-gift',
         //    	message: "Welcome to <b>Paper Dashboard</b> - a beautiful Bootstrap freebie for your next project."

         //    },{
         //        type: 'success',
         //        timer: 4000
         //    });

    	});
	</script>

</html>
