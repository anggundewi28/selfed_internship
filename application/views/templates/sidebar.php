<div class="sidebar" data-background-color="white" data-active-color="danger">

<!--
	Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
	Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
-->

	<div class="sidebar-wrapper">
        <div class="logo">
            <a href="https://selfed.co" class="simple-text">
                Selfed.co
            </a>
        </div>

        <ul class="nav">
            <li class="<?php if($this->uri->segment(1) =='' || $this->uri->segment(1) =='dashboard') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="<?php if($this->uri->segment(1) =='members') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>members">
                    <i class="ti-user"></i>
                    <p>Member Management</p>
                </a>
            </li>
            <li class="<?php if($this->uri->segment(1) =='modules') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>modules">
                    <i class="ti-pencil-alt2"></i>
                    <p>Module Management</p>
                </a>
            </li>
			<li class="<?php if($this->uri->segment(1) =='category') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>category">
                    <i class="ti-file"></i>
                    <p>Category Management</p>
                </a>
            </li>
            <!-- <li class="<?php if($this->uri->segment(1) =='news') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>news">
                    <i class="ti-world"></i>
                    <p>News Management</p>
                </a>
            </li> -->
            <li class="<?php if($this->uri->segment(1) =='articles') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>articles">
                    <i class="ti-notepad"></i>
                    <p>Articles Management</p>
                </a>
            </li>
            <li class="<?php if($this->uri->segment(1) =='ads') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>ads">
                    <i class="ti-rocket"></i>
                    <p>Ads Management</p>
                </a>
            </li>
            <li class="<?php if($this->uri->segment(1) =='users') echo 'active'; else {echo ''; } ?> ">
                <a href="<?php echo base_url(); ?>users">
                    <i class="ti-headphone-alt"></i>
                    <p>Admin Management</p>
                </a>
            </li>
            
        </ul>
	</div>
</div>
