<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>

                <li>
                    <a href="http://www.twitter.com">
                        <span class="ti-twitter-alt" title="Twitter Selfed.co"></span>
                    </a>
                </li>
                <li>
                    <a href="http://facebook.com">
                       <span class="ti-facebook" title="Facebbok Selfed.co"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.youtube.com">
                        <span class="ti-youtube" title="Youtube Selfed.co"></span>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
        </div>
    </div>
</footer>