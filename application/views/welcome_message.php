<!DOCTYPE html>
<html>
    <head>
       <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
        <title>Cofounder Wannabe</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css" />
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/form-style.css" />
		<link rel="stylesheet" type="text/css" href="css/color-style.css" />
		<link rel="stylesheet" type="text/css" href="css/box-style.css" />
		
	</head>
	<body >
		<button class="bn-small " id="app_main_login">LOGIN <i class="fa fa-key"></i></button>
		<button class="bn-small bc-red" id="app_main_signup">SIGNUP</button>
		
		<div id="app_main_loginform">
			<input type="text" class="form-center-white" id="app_main_loginform_email" placeholder="Email Address" />
			<input type="password" class="form-center-white" id="app_main_loginform_pass" placeholder="Password" />
			<button class="form-center-1-button bc-orange" id="app_main_loginform_send">LOGIN</button>
		</div>
		
		<!-- Popout Message -->
		<div id="app_popout" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.55);">
			<div class="center_frame" >
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-box" id="app_popout_state">
						
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		
		<div class="bgimg-front">
			<div class="caption">
				<h3>Connect, meet and collaborate<br/>with like-minded entrepreneurs.</h3>
			</div>
		</div>
		
		<!-- SignUp -->
		<div id="app_main_reg" class="center_box" style="display: none; background: rgba(0, 0, 0, 0.75);">
			<div class="center_frame" >
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
					<div class="col-md-4 col-sm-4 col-xs-10 clean-straight-box" style="position: relative;">
						<button id="app_main_reg_close" class="btn btn-circle bc-red" style="position: absolute; top: -10px; right: -5px;"><i class="fa fa-times"></i></button>
					
						<input type="text" id="app_main_reg_email" placeholder="Email Address" class="form-center" />
						<input type="text" id="app_main_reg_name" placeholder="Fullname" class="form-center" />
						<input type="text" id="app_main_reg_phone" placeholder="Phone Number" class="form-center" />
						<button class="form-center-1-button bc-green"  id="app_main_reg_send">
							SIGNUP <i class="fa fa-check"></i>
						</button>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-1"></div>
				</div>
			</div>
		</div>
		
		
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/numeric.js"></script>
		
		<script type="text/javascript" src="script/app_main_reg.js"></script>
		
		<script>
			$("#app_main_reg_phone").numeric();
		
			
			
			$("#app_main_login").click(function(){
				$("#app_main_login").fadeOut();
				$("#app_main_loginform").slideDown();
			});
			
			$("#app_main_loginform_send").click(function(){
				$("#app_main_loginform").slideUp();
				$("#app_main_login").fadeIn();
			});
			
			function popout(msg){
				$("#app_popout").fadeIn();
				$("#app_popout").css("display", "table");
				$("#app_popout_state").html(msg);
			}
			
			$("#app_popout").click(function(){
				$(this).fadeOut();
			});
			
			
		</script>
    </body>
</html>