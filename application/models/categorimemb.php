<?php

    class Categorimemb extends CI_Model {
    
        public function tampil_data(){ 
            return $this->db->get('category')->result();
        }
     
        function input_data($data,$table){
            $this->db->insert($table,$data);
        }
		
		public function get_by_id($id){
            return $this->db->get_where('category',array('id'=>$id))->row();
        }
		
		public function delCategory($id){ //fungsi delete berdasarkan id
            $this->db->where('id',$id); //pencocokan id, dimana id_transaksi == inputan $id_transaksi
            $this->db->delete('category'); //eksekusi
            return;
        }
		
		function update_data($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
	}	
}

?>