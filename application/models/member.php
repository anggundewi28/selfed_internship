<?php

    class Member extends CI_Model {
    
        public function delMember($id){ //fungsi delete berdasarkan id
            $this->db->where('id',$id); //pencocokan id, dimana id_transaksi == inputan $id_transaksi
            $this->db->delete('member'); //eksekusi
            return;
        }

        public function tampil_data(){ 
            return $this->db->get('member')->result();
        }
     
        function input_data($data,$table){
            $this->db->insert($table,$data);
        }

        function edit_data($where,$table){		
            return $this->db->get_where($table,$where);
        }

        public function get_by_id($id){
            return $this->db->get_where('member',array('id'=>$id))->row();
        }

        function update_data($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }	

        public function get_member_keyword($keyword)
        {
            
            $this->db->select('*');
            $this->db->from('member');
            $this->db->like('name', $keyword);
            $this->db->or_like('phone',$keyword);
            return $this->db->get()->result();
        }

        function login($name, $password)  
      {  
           $periksa = $this->db->get_where('member',array('name'=>$name,'password'=>$password));
		   
		   if($periksa->num_rows()>0){
				return 1;
		   }
		   else{
			   return 0;
		   }
      }  
}

?>