<?php 

class DataManage extends CI_Model {
	
	public function register($data){
		$this->db->insert('member',$data);
	}
	
	public function fetch_all($table_name){
		$allData = $this->db->get($table_name);
		return $allData->result();
	}

	public function get_num_rows($table_name){
		$allData = $this->db->get($table_name);
		return $allData->num_rows();
	}

	public function fetch_all_limit($table_name, $limit, $offset){

		$query = $this->db->get($table_name, $limit, $offset);
		
		if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;

		return $query->result();
	}

	public function fetch_article_where($id){
		$value = $this->db->get_where('articles', array('article_id' => $id));
		return $value;
	}

	public function fetch_member_where($id){
		$value = $this->db->get_where('member', array('id' => $id));
		return $value;
	}

	public function fetch_modules_where($id){
		$value = $this->db->get_where('modules', array('id' => $id));
		return $value;
	}

	public function fetch_ad_where($id){
		$value = $this->db->get_where('ads', array('ad_id' => $id));
		return $value;
	}

	public function fetch_news_where($id){
		$value = $this->db->get_where('news', array('news_id' => $id));
		return $value;
	}

	public function fetch_ads_where($id){
		$value = $this->db->get_where('news', array('ad_id' => $id));
		return $value;
	}

	public function fetch_user_where($id){
		$value = $this->db->get_where('users', array('user_id' => $id));
		return $value;
	}

	public function fetch_member_where($id){
		$value = $this->db->get_where('member', array('id' => $id));
		return $value;
	}

	public function delete_member($id){
		$this->db->delete('member', array('id' => $id));
		return $delete;
	}

	public function delete_article($id)
	{
		$this->db->delete('articles', array('article_id' => $id));
		return $delete;
	}

	public function delete_news($id)
	{
		$delete = $this->db->delete('news', array('news_id' => $id));
		return $delete;
	}

	public function delete_ad($id)
	{
		$delete = $this->db->delete('ads', array('ad_id' => $id));
		return $delete;
	}

	public function delete_user($id)
	{
		$delete = $this->db->delete('users', array('user_id' => $id));
		return $delete;
	}

	public function update_news($id, $data)
	{
		$this->db->where('news_id', $id);
		$update = $this->db->update('news', $data);
		return $update;
	}

	public function update_user($id, $data)
	{
		$this->db->where('user_id', $id);
		$update = $this->db->update('users', $data);
		return $update;
	}

	public function update_ad($id, $data)
	{ 
		$this->db->where('ad_id', $id);
		$update = $this->db->update('ads', $data);
		return $update;
	}

	public function update_member($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update('member', $data);
		return $update;
	}
	
	public function update_article($id, $data)
	{
		$this->db->where('article_id', $id);
		$update = $this->db->update('articles', $data);
		return $update;
	}
	
	public function save_module($data){
		$this->db->insert('modules',$data);
	}

	public function save_user($data){
		$this->db->insert('users',$data);
	}
	
	public function count_email($email){
		$query = $this->db->query("SELECT id FROM member WHERE email = '".$email."'");
		return $query->num_rows();
	}
	
	public function login_email($email, $password){
		$query = $this->db->query("SELECT id FROM member WHERE email = '".$email."' AND password = '".$password."' ");
		return $query->row();
	}

	public function valid_update_user($id, $password){
		$query = $this->db->query("SELECT id FROM member WHERE id = '".$id."' AND password = '".$password."' ");
		return $query->num_rows();
	}
	
	public function id_from_email($email){
		$query = $this->db->query("SELECT id FROM member WHERE email = '".$email."' ");
		return $query->row();
	}
	
	public function load_categories(){
		$query = $this->db->query("SELECT *
									FROM category 
									WHERE status = '1' ");

      	return $query->result();
	}

	public function load_most_view_modules(){
		$query = $this->db->query("SELECT modules.id, modules.id_member, modules.id_cat, modules.path, modules.thumb, modules.title, modules.description, modules.status, modules.stamp, member.name AS member_name, category.name AS category_name, (SELECT GROUP_CONCAT(favorite.id_member) FROM favorite WHERE favorite.id_module = modules.id) AS member_fav, (SELECT AVG(rating.star) FROM rating WHERE rating.id_module = modules.id) AS star FROM modules INNER JOIN member ON member.id = modules.id_member INNER JOIN category ON category.id = modules.id_cat ORDER BY modules.id DESC limit 0,10");

      	return $query->result();
	}

	public function load_all_modules($offset, $search = ''){
		if($search != ''){
			$search = " WHERE modules.title LIKE '%". $search ."%'";
		}
		$query = $this->db->query("SELECT modules.id, modules.id_member, modules.id_cat, modules.path, modules.thumb, modules.title, modules.description, modules.status, modules.stamp, member.name AS member_name, category.name AS category_name, (SELECT GROUP_CONCAT(favorite.id_member) FROM favorite WHERE favorite.id_module = modules.id) AS member_fav, (SELECT AVG(rating.star) FROM rating WHERE rating.id_module = modules.id) AS star FROM modules INNER JOIN member ON member.id = modules.id_member INNER JOIN category ON category.id = modules.id_cat $search ORDER BY modules.id DESC limit $offset,25");

      	return $query->result();
	}

	public function load_all_modules_by_teacher($offset,$id_member){
		$query = $this->db->query("SELECT modules.id, modules.id_member, modules.id_cat, modules.path, modules.thumb, modules.title, modules.description, modules.status, modules.stamp, member.name AS member_name, category.name AS category_name, (SELECT GROUP_CONCAT(favorite.id_member) FROM favorite WHERE favorite.id_module = modules.id) AS member_fav, (SELECT AVG(rating.star) FROM rating WHERE rating.id_module = modules.id) AS star FROM modules INNER JOIN member ON member.id = modules.id_member INNER JOIN category ON category.id = modules.id_cat WHERE modules.id_member = '$id_member' ORDER BY  modules.id DESC limit $offset,25");

      	return $query->result();
	}

	public function load_fav($offset,$username){
		$query = $this->db->query("SELECT *
									FROM modules WHERE id_member = (SELECT id FROM member WHERE email ='$username') ORDER BY id DESC limit $offset,25");

      	return $query->result();
	}
	
	public function load_news(){
		$query = $this->db->query("SELECT *
									FROM news ORDER BY id DESC limit 0,10");

      	return $query->result();
	}

	public function ban_user($user_id, $value){
		$query = $this->db->query("UPDATE member 
									SET status = $value 
									WHERE id = $user_id ") ;

      return $query;
	}

	public function edit($id,$data){
		$this->db->where('id', $id);
		$update = $this->db->update('member', $data);
		return $update;
	}

	public function edit_member($user_id, $data){
		$query = $this->db->query("UPDATE member 
									SET name = '".$data['name']."', email = '".$data['email']."', phone = '".$data['phone']."'
									WHERE id = '".$user_id."' ");
      return $query;
	}

	public function update_pass($user_id, $pass){
		$query = $this->db->query("UPDATE member 
									SET password = '".$pass."'
									WHERE id = '".$user_id."' ");
      return $query;
	}

	public function ban_tutorial($tut_id, $value){
		$query = $this->db->query("UPDATE modules 
									SET status = $value 
									WHERE id = $tut_id " );

      return $query;
	}

	public function save_article($data) {
		$query = $this->db->insert('articles',$data);
		return $query;
	}

	public function save_ad($data) {
		$query = $this->db->insert('ads',$data);
		return $query;
	}

	public function save_news($data) {
		$query = $this->db->insert('news',$data);
		return $query;
	}

	public function load_member($id){
		$query = $this->db->query("SELECT * FROM member WHERE id = '".$id."'");
		return $query->result();
	}

	public function verifyLogin($username, $password) 
	{
		$query = $this->db->where('username', $username)->where('password', sha1($password))->where('status', '1')->limit(1)->get('users');

		$num_rows = $query->num_rows();

		if ( $num_rows > 0 ) {
			return $query->row();
		} else {
			return NULL; 
		}
		
		
	}

	public function load_fav_by_id_member($offset, $id_member){
		$query = $this->db->query("SELECT modules.id, modules.id_member, modules.id_cat, modules.path, modules.thumb, modules.title, modules.description, modules.status, modules.stamp, member.name AS member_name, category.name AS category_name, (SELECT AVG(rating.star) FROM rating WHERE rating.id_module = modules.id) AS star FROM favorite INNER JOIN modules ON modules.id = favorite.id_module INNER JOIN member ON member.id = modules.id_member INNER JOIN category ON category.id = modules.id_cat WHERE favorite.id_member = '".$id_member."' ORDER BY id DESC limit $offset,25");
		return $query->result();
	}

	public function save_fav($id_member, $id_module) {
		$query = $this->db->query("INSERT INTO `favorite` (`id_member`, `id_module`, `stamp`) VALUES ('".$id_member."','".$id_module."',NOW())");
		return $query;
	}

	public function delete_fav($id_member, $id_module) {
		$query = $this->db->query("DELETE FROM `favorite` WHERE id_member = '".$id_member."' AND id_module = '".$id_module."'");
		return $query;
	}

	public function check_fav($id_member, $id_module) {
		$query = $this->db->query("SELECT * FROM `favorite` WHERE id_member = '".$id_member."' AND id_module = '".$id_module."'");
		return $query->num_rows();
	}

	public function save_rating($id_member, $id_module, $star) {
		$query = $this->db->query("INSERT INTO `rating` (`id_member`, `id_module`, `star`, `stamp`) VALUES ('".$id_member."','".$id_module."','".$star."',NOW())");
		return $query;
	}
}

?>