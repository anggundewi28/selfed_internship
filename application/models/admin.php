<?php

    class Admin extends CI_Model {
    
        public function getMember($member)
        {
            $data = $this->db->get($member);
            return $data->result_array();
        }

        public function getModules($modules)
        { 
            $data = $this->db->get($modules);
            $this->db->limit(5);
            return $data->result_array();
        }

        public function getArticles($articles)
        {
            $data = $this->db->get($articles);
            $this->db->limit(5);
            return $data->result_array();
        }

        public function getAds($ads)
        {
            $data = $this->db->get($ads);
            $this->db->limit(5);
            return $data->result_array();
        }

        public function get_count($member)
        {
            $data = $this->db->get($member);
            $num = $data->num_rows();
            return $num;
        }

        public function get_countmodule($modules)
        {
            $data = $this->db->get($modules);
            $num = $data->num_rows();
            return $num;
        }

        public function get_countarticle($articles)
        {
            $data = $this->db->get($articles);
            $num = $data->num_rows();
            return $num;
        }

        public function get_countads($ads)
        {
            $data = $this->db->get($ads);
            $num = $data->num_rows();
            return $num;
        }
    }

?>