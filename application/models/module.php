<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Module extends CI_Model {

        public $variable;

        public function __construct()
        {
            parent::__construct();
            
        }
    
        public function delModule($id){ //fungsi delete berdasarkan id
            $this->db->where('id',$id); //pencocokan id, dimana id_transaksi == inputan $id_transaksi
            $this->db->delete('modules'); //eksekusi
            return;
        }

        public function tampil(){
            return $this->db->get('modules')->result();
        }

        public function update($where,$data,$table)
        {
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        public function get_by_id($id){
            return $this->db->get_where('modules',array('id'=>$id))->row();
        }

        function tambah_data($data_module){
            $this->db->insert('modules',$data_module);
        }

        function edit_data($data,$data_module){
            $this->db->where($data);
            $this->db->update('modules',$data_module);
        }

        public function get_module_keyword($keyword)
        {
            
            $this->db->select('*');
            $this->db->from('modules');
            $this->db->like('title', $keyword);
            $this->db->or_like('description',$keyword);
            return $this->db->get()->result();
        }

}
