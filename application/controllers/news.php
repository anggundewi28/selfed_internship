<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class News extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		// $this->authent->checkLogin();
		// $this->notifications->checkDraft();

	}

	public function index()
	{
		$data['news'] = $this->DataManage->fetch_all('news');

		$title = "News Management"; 
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/news-table', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['news'] = $this->DataManage->fetch_news_where($id)->row();
			$title = "Edit News";
		} else {
			$title = "Add New News";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/news-form', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function delete_news($id)
	{
		$del = $this->DataManage->delete_news($id);
		if( $del ) {
			$this->session->set_flashdata('delete-success', 'News Deleted');
		} else {
			$this->session->set_flashdata('delete-fail', 'Failed to Delete News');
		}

		redirect('/news');
		
	}

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

}