<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();
		session_start();
		$this->load->library('authent');
		// $this->authent->checkLogin();
		// $this->notifications->checkDraft();

	}

	public function index()
	{
		$data['users'] = $this->DataManage->fetch_all('users');
		$title = "Admin Management";
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/users-table', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['users'] = $this->DataManage->fetch_user_where($id)->row();
			$title = "Edit Admin";
		} else {
			$title = "Add New Admin";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/user-form', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function delete_user($id)
	{
		$del = $this->DataManage->delete_user($id);
		if( $del ) {
			$this->session->set_flashdata('delete-success', 'Admin Deleted');
		} else {
			$this->session->set_flashdata('delete-fail', 'Failed to Delete Admin');
		}

		redirect('/users');
		
	}

	public function logout()
	{
		session_destroy();
		redirect('/users/login');
	}

	public function verify() 
	{
		$res = $this->DataManage->verifyLogin($this->input->post('username'), $this->input->post('password'));

		 if ($res->user_id > 0) {
			$_SESSION['username'] = $this->input->post('username');
			$_SESSION['role'] = $res->role;
			$_SESSION['user_id'] = $res->user_id;
		 }
		 else {
		 	$this->session->set_flashdata('fail', 'Wrong username / password !');
			redirect('/users/login/');
		}
			redirect('/dashboard');


	}

	public function login()
	{
		if(isset($this->session->username)) {
			redirect('/dashboard');
		}

		$data = array();
		$this->load->view('pages/login-as', $data);
	}

	public function loginadmin()
	{
		if(isset($this->session->username)) {
			redirect('/dashboard');
		}

		$data = array();
		$this->load->view('pages/admin-login', $data);
	}

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}
 
}