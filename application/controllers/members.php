<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('member');
		// $this->notifications->checkDraft();

	}

	public function index()
	{
		//PAGINATION
		$member_rows = $this->DataManage->get_num_rows('member');
		$data['members'] = $this->DataManage->fetch_all('member');
		$config['base_url'] = base_url('members/index/');
		$config['total_rows'] = $member_rows;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3; 
		$offset = $this->uri->segment(3);

		$data['members'] = $this->DataManage->fetch_all_limit('member', $config['per_page'],$offset);
		
		$this->pagination->initialize($config);

		$title = "Member Management";
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/member-table', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	public function edit_data_member(){
		$title = "Edit Data";
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/editmember', $data, true);
		$data['membera'] = $this->member->edit_data($where,'member')->result();
		$this->load->view('templates/main', $data);
	}

	

	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['member'] = $this->DataManage->fetch_member_where($id)->row();
			$title = "Edit Member";
		} else {
			$title = "Add New Member";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/member-form', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function addMembers()
	{
		$this->load->view('member-form');
	}

	public function tambah(){
		$this->load->view('pages/member-form');
	}

	public function tambah_aksi(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
 
		$data = array(
			'email' => $email,
			'password' => $password,
			'name' => $name,
			'phone' => $phone,
			'status' => '1'
			);
		$this->member->input_data($data,'member');
		redirect('/members');
	}

	public function addMembersDb()
	{
		$data = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone')
			);
	
			if (is_numeric($id)) {
				$query = $this->DataManage->edit($id, $data);
			} else {
				$query = $this->DataManage->register($data);	
			}
			
			if($query) {
				redirect('/members');
			}
	$this->DataManage->register($data); 

	redirect('/members'); 
	}

	function edit($id){
		$where = array('id' => $id);
		$query = $this->DataManage->edit($id, $data);
		$query = $this->DataManage->register($data);	
	}

	public function delMember($id){ //fungsi delete
		$this->load->model('member'); //load model
		$this->member->delMember($id); //ngacir ke fungsi delTransaksi
		redirect('/members'); //redirect
	 
	}

	function update(){
		$id=$this->input->post('id');
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		$name=$this->input->post('name');
		$phone=$this->input->post('phone');
	 
		$data = array(
			'email' => $email,
			'password' => $password,
			'name' => $name,
			'phone' => $phone
		);
	 
		$where = array(
			'id' => $id
		);
	 
		$this->member->update_data($where,$data,'member');
		redirect('/members');
	}

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}
	
	public function loginmember()
	{
		$this->load->view('pages/users-login');  
	}

	public function login_validation() 
	{
		$this->load->library('form_validation');  
           $this->form_validation->set_rules('name', 'name', 'required');  
           $this->form_validation->set_rules('password', 'Password', 'required');  
           if($this->form_validation->run())  
           {  
                //true  
                $name = $this->input->post('name');  
                $password = $this->input->post('password');  
                //model function  
                $this->load->model('member');  
                if($this->member->can_login($name, $password))  
                {  
                     $session_data = array(  
                          'name'     =>     $name  
                     );  
                     $this->session->set_userdata($session_data);  
                     redirect('/members/enter');  
                }  
                else  
                {  
                     $this->session->set_flashdata('error', 'Invalid Name and Password');  
                     redirect('/members/loginmember');  
                }  
           }  
           else  
           {  
                //false  
                $this->loginmember();  
           }  
	}

	public function enter()
	{
		if($this->session->userdata('name') != '')  
           {  
                redirect('/dashboardmemb');
           }  
           else  
           {  
                redirect('/members/loginmember');  
           }  
	}
	
	function logout()  
      {  
           $this->session->sess_destroy();  
           redirect('/');  
      }
	  
	function login()  
      {  
           if(isset($_POST['submit'])){
			   $name = $this->input->post('name');
			   $password = $this->input->post('password');
			   $berhasil = $this->member->login($name,$password);
			   if($berhasil == 1){
				   $this->session->set_userdata(array('status_login'=>'success'));
				   redirect('/dashboardmemb'); 
			   }
			   else{
				   redirect('/members/loginmember');
			   }
		   }
		   else{
			   $this->load->view('/pages/users-login');
		   }
	  }
	  
		public function search()
		{
			$title = "Member Management";
			$keyword = $this->input->post('keyword');
			$data['members']=$this->member->get_member_keyword($keyword);
			$data['sidebar']= $this->sidebar();
			$data['header']= $this->header($title);
			$data['content'] = $this->load->view('pages/member-table', $data, true);
			$data['footer'] = $this->footer();
			
			$this->load->view('templates/main', $data);
		}

}