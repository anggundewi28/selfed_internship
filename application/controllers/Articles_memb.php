<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_memb extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();
		// $this->notifications->checkDraft();

	}

	public function index()
	{

		//PAGINATION
		$articles_rows = $this->Data->get_num_rows('articles');
		$config['base_url'] = base_url('articles_memb/index/');
		$config['total_rows'] = $articles_rows;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);

		$data['articles'] = $this->Data->fetch_all_limit('articles', $config['per_page'],$offset);
		
		$this->pagination->initialize($config);

		$title = "Articles Management";
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/articles-table-memb', $data, true);
		$this->load->view('templates/main', $data);
	}
 
	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['articles'] = $this->Data->fetch_article_where($id)->row();
			$title = "Edit Article";
		} else {
			$title = "Add New Article";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/articles-form-memb', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function delete_article($id)
	{
		$del = $this->Data->delete_article($id);
		if( $del ) {
			$this->session->set_flashdata('delete-success', 'News Deleted');
		} else {
			$this->session->set_flashdata('delete-fail', 'Failed to Delete News');
		}

		redirect('/articles_memb');
	}

	public function add()
	{
		$config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'jpg|jpeg|png|PNG|JPG|JPEG';
        $config['max_size']             = 5000;

        $this->load->library('upload', $config);
    
        $this->upload->do_upload('article_cover');
        $image_upload = $this->upload->data();

		$article_cover = $_FILES["article_cover"]["name"];
		
		$id = $this->input->post('id');

		if( is_numeric($id) && empty($article_cover)) {

			$data = array(
				'article_title' => $this->input->post('title'),
				'article_content' => $this->input->post('content'),				
				'article_status' => '1'
			);	
		
		} 
		else {

			$data = array(
				'article_title' => $this->input->post('title'),
				'article_content' => $this->input->post('content'),
				'article_cover' => $config['upload_path'].$article_cover,
				'article_status' => '1'
			);	
		}
		
		
				
		if (is_numeric($id)) {
			$query = $this->Data->update_article($id, $data);
		} else {
			$query = $this->Data->save_article($data);	
		}
		
		if($query) {
			redirect('/articles_memb');
		}
	}

			

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar _memb', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

}