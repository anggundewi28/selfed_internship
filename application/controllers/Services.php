<?php 
header('Access-Control-Allow-Origin: *');
class Services extends CI_Controller {  

	public function index() {
		$flag = $this->input->post('flag');

			//Register new member
		if($flag == "register"){
			$email = $this->input->post('email');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$password = $this->input->post('password');
			$stamp = date("Y-m-d H:i:s");

			$check_email = $this->DataManage->count_email($email);

			if($check_email === 0){
				$data = array(
					'email' => $email,
					'password' => $password,
					'name' => $name,
					'phone' => $phone,
					'status' => 1,
					'regdate' => $stamp,
				);

				$this->DataManage->register($data);

				echo json_encode(array(
					'valid' => 1
				)); 
			}
			else{
				echo json_encode(array(
					'valid' => 2
				)); 
			}
		}

			//Check the idle proccess
		if($flag == "idle"){
			$username = $this->input->post('username');
			$check_email = $this->DataManage->count_email($username);

			if($check_email > 0){
				echo json_encode(array(
					'valid' => 1
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}

			//load_member
		if($flag == "load_member"){
			$id = $this->input->post('id');

			$list = $this->DataManage->load_member($id);

			echo json_encode(array(
				'valid' => 1,
				'list' => $list
			));
		}

			//Login
		if($flag == "login"){
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$check_email = $this->DataManage->login_email($email, $password);

			if(!empty($check_email)){
				echo json_encode(array(
					'valid' => 1,
					'email' => $email,
					'id' => $check_email->id
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}

			//upload video module
		if($flag == "upload_video_module"){
			$username = $this->input->post('username');
			$realMinutes = "";
			$realSeconds = "";

			if ( 0 < $_FILES['file']['error'] ) {
						//Response Message
				echo json_encode(array(
					'valid' => 2,
					'error' => $_FILES['file']['error']
				));
			}
			else {
				$filename = $_FILES['file']['name'];
				$stamp = date("Y-m-d H:i:s");
				$prod =  $this->rand_string(35);
				$explode_file = explode(".", $filename);
				$extension = end($explode_file);

				$newfilename = $username."-".$prod .".".$extension;

				$uploaddir = 'modules/';
				$uploadfile = $uploaddir . $newfilename;

				move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

				$filename_convert = pathinfo($newfilename, PATHINFO_FILENAME);

				$originalName = "selfed-".$filename_convert;

				$convertVideo = "selfed-".$filename_convert.".mp4";

				$convertThumb = "selfed-".$filename_convert.".jpg";

				$file_converted = $uploadfile;

				$result = shell_exec("/usr/bin/ffmpeg -i ".$file_converted." 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//");

				$duration = explode(":",$result);

				$duration_in_seconds = $duration[0]*3600 + $duration[1]*60+ round($duration[2]);

				$thumb_time = gmdate("H:i:s", ($duration_in_seconds/2));



					// duration in seconds; half the duration = middle

				$duration = explode(":",$result);

				$durationInSeconds = $duration[0]*3600 + $duration[1]*60+ round($duration[2]);

				$durationMiddle = $durationInSeconds/2;



					// recalculte to minutes and seconds

				$minutes = $durationMiddle/60;

				$realMinutes = floor($minutes);

				$realSeconds = round(($minutes)*60);

					//Convert and create thumb
				
				shell_exec("/usr/bin/ffmpeg -i ".$uploadfile." -codec:a libmp3lame -b:a 128k -ss ".$thumb_time.".000  -vframes 1 modules/thumbs/".$convertThumb." -vf scale=360:240:gamma=1 modules/".$convertVideo." 2>&1");


				// shell_exec("/usr/bin/ffmpeg -i ".$uploadfile." -codec:a libmp3lame -b:a 128k -ss ".$thumb_time.".000  -vframes 1 modules/thumbs/".$convertThumb." -vf scale=320:-1 modules/".$convertVideo." 2>&1");

					//shell_exec("/usr/bin/ffmpeg -i modules/thumbs/".$convertThumb." -vf scale=360:240:gamma=1 modules/thumbs/".$convertThumb." 2>&1");

				unlink($uploadfile);

				echo $originalName;
			}
		}	

		//upload document module
		if($flag == "upload_document_module"){
			$username = $this->input->post('username');

			if ( 0 < $_FILES['file']['error'] ) {
						//Response Message
				echo json_encode(array(
					'valid' => 2,
					'error' => $_FILES['file']['error']
				));
			}
			else {
				$filename = $_FILES['file']['name'];
				$stamp = date("Y-m-d H:i:s");
				$prod =  $this->rand_string(35);
				$explode_file = explode(".", $filename);
				// $extension = end($explode_file);

				$newfilename = $username."-".$prod .".pdf";

				$uploaddir = 'modules/';
				$uploadfile = $uploaddir . $newfilename;

				move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

				// $filename_convert = pathinfo($newfilename, PATHINFO_FILENAME);

				// $originalName = "selfed-".$filename_convert;

				// $convertThumb = "selfed-".$filename_convert.".jpg";

				// $file_converted = $uploadfile;

					//shell_exec("/usr/bin/ffmpeg -i modules/thumbs/".$convertThumb." -vf scale=360:240:gamma=1 modules/thumbs/".$convertThumb." 2>&1");

				// unlink($uploadfile);

				echo $uploadfile;
			}
		}

		//upload document module
		if($flag == "upload_image_module"){
			$username = $this->input->post('username');

			if ( 0 < $_FILES['file']['error'] ) {
						//Response Message
				echo json_encode(array(
					'valid' => 2,
					'error' => $_FILES['file']['error']
				));
			}
			else {
				$filename = $_FILES['file']['name'];
				$stamp = date("Y-m-d H:i:s");
				$prod =  $this->rand_string(35);
				$explode_file = explode(".", $filename);
				// $extension = end($explode_file);

				$newfilename = $username."-".$prod .".jpg";

				$uploaddir = 'modules/';
				$uploadfile = $uploaddir . $newfilename;

				move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

				// $filename_convert = pathinfo($newfilename, PATHINFO_FILENAME);

				// $originalName = "selfed-".$filename_convert;

				// $convertThumb = "selfed-".$filename_convert.".jpg";

				// $file_converted = $uploadfile;

					//shell_exec("/usr/bin/ffmpeg -i modules/thumbs/".$convertThumb." -vf scale=360:240:gamma=1 modules/thumbs/".$convertThumb." 2>&1");

				// unlink($uploadfile);

				echo $uploadfile;
			}
		}
			//Converted thumbnail
		if($flag == "crop_thumbnail"){
			$file = $this->input->post('file');

			$prod =  $this->rand_string(35);
			$explode_file = explode(".", $file);
			$extension = end($explode_file);
			$newfilename = $prod .".".$extension;
			$newthumb_filename = $prod ."_thumb.".$extension;

			$this->load->library('image_lib');

			$uploaddir = 'modules/thumbs/';
			$uploadfile = $uploaddir . $file;

			list($width, $height) = getimagesize($uploadfile);

			if($width >= $height){
				$dim = "height";
			}
			else{
				$dim = "width";
			}

			$config['image_library'] = 'gd2';
			$config['source_image'] = $uploadfile;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim'] = $dim;
			$config['quality'] = "90%";
			$config['width']     = 256;
			$config['height']   = 256;

			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->resize();

			$config_thumb['image_library'] = 'gd2';
			$config_thumb['source_image'] = "modules/thumbs/".$newthumb_filename;
			$config_thumb['create_thumb'] = FALSE;
			$config_thumb['maintain_ratio'] = FALSE;
			$config_thumb['master_dim'] = $dim;
			$config_thumb['quality'] = "90%";
			$config_thumb['width']     = 256;
			$config_thumb['height']   = 256;
			$config_thumb['x_axis'] = '0';
			$config_thumb['y_axis'] = '0';

			$this->image_lib->clear();
			$this->image_lib->initialize($config_thumb);
			$this->image_lib->crop();

			echo json_encode(array(
				'thumb' => $newthumb_filename
			));
		}

			//Forgot Password
		if($flag == "forgot_password"){
			$email = $this->input->post('email');

			$check_email = $this->DataManage->count_email($email);
			if($check_email > 0){
				echo json_encode(array(
					'valid' => 1
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}

		//VIEW COUNTER for MODULE
		if($flag == "count_view") {

			$data['module_id'] = $this->input->post('module_id');
			$counter = $this->DataManage->count_view($data);
			
			echo json_encode(array(
				'message' => 'counted'
			));

		}

		//Save Module
		if($flag == "save_module"){
			$username = $this->input->post('username');
			$catid = $this->input->post('catid');
			$type = $this->input->post('type');
			$file = $this->input->post('file');
			$thumb = $this->input->post('thumb');
			$title = $this->input->post('title');
			$desc = $this->input->post('desc');

			$check_email = $this->DataManage->count_email($username);
			if($check_email > 0){
				$userId = $this->DataManage->id_from_email($username);
				$stamp = date("Y-m-d H:i:s");
				$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/selfed_services/modules/thumbs/';
				$uploadfile = $uploaddir . $thumb;

				list($width, $height) = getimagesize($uploadfile);

				if($width >= $height){
					$dim = "height";
				}
				else{
					$dim = "width";
				}

				$stamp = date("Y-m-d H:i:s");

				$this->load->library('image_lib');

				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadfile;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['master_dim'] = $dim;
				$config['quality'] = "90%";
				$config['width']     = 256;
				$config['height']   = 256;

				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

				$config_thumb['image_library'] = 'gd2';
				$config_thumb['source_image'] = $uploadfile;
				$config_thumb['create_thumb'] = FALSE;
				$config_thumb['maintain_ratio'] = FALSE;
				$config_thumb['master_dim'] = $dim;
				$config_thumb['quality'] = "90%";
				$config_thumb['width']     = 256;
				$config_thumb['height']   = 256;
				$config_thumb['x_axis'] = '0';
				$config_thumb['y_axis'] = '0';

				$this->image_lib->clear();
				$this->image_lib->initialize($config_thumb);
				$this->image_lib->crop();

				$data = array(
					'id_member' => $userId['id'],
					'id_cat' => $catid,
					'path' => $file,
					'thumb' => $thumb,
					'title' => mysql_real_escape_string($title),
					'description' => mysql_real_escape_string($desc),
					'stamp' => $stamp
				);

				$this->DataManage->save_module($data);

				echo json_encode(array(
					'valid' => 1
				));
			}
			else{

				echo json_encode(array(
					'valid' => 0
				));
			}
		}

			//load_categories
		if($flag == "load_categories"){
			$cat = $this->DataManage->load_categories();

			echo json_encode(array(
				'list' => $cat
			));
		}

			//load_all_modules
		if($flag == "load_all_modules"){
			$page = $this->input->post('page');

			$offset = ($page - 1) * 25;

			$modules = $this->DataManage->load_all_modules($offset);

			echo json_encode(array(
				'list' => $modules
			));
		}

			//load_search_modules
		if($flag == "load_search_modules"){
			$page = $this->input->post('page');
			$search = $this->input->post('search');
			$type = $this->input->post('type');

			$offset = ($page - 1) * 25;

			$modules = $this->DataManage->load_all_modules($offset,$search,$type);

			echo json_encode(array(
				'list' => $modules
			));
		}
			//load_fav
		if($flag == "load_fav"){
			$page = $this->input->post('page');
			$id_member = $this->input->post('id_member');

			$offset = ($page - 1) * 25;

			$modules = $this->DataManage->load_fav($offset);

			echo json_encode(array(
				'list' => $modules
			));
		}
			//load_all_modules_by_teacher
		if($flag == "load_all_modules_by_teacher"){
			$page = $this->input->post('page');
			$username = $this->input->post('username');

			$offset = ($page - 1) * 25;

			$modules = $this->DataManage->load_all_modules_by_teacher($offset,$username);

			echo json_encode(array(
				'list' => $modules
			));
		}

			//load_news
		if($flag == "load_news"){
			$list = $this->DataManage->load_news();

			echo json_encode(array(
				'list' => $list
			));
		}

		if($flag == "load_all_articles") {
			$art = $this->DataManage->fetch_all('articles');
			$page = $this->input->post('page');

			$offset = ($page - 1) * 15;

			echo json_encode(array(
				'articles' => $art
			));
		}

		if($flag == "load_this_article") {
			$id = $this->input->post('article_id');
			$art = $this->DataManage->fetch_article_where($id);

			echo json_encode(array(
				'article' => $art
			));
		}

		if($flag == "load_all_ads") {
			$ad = $this->DataManage->fetch_all('ads');

			echo json_encode(array(
				'ads' => $ad
			));
		}

		if($flag == "load_this_ad") {
			$id = $this->input->post('ad_id');
			$ad = $this->DataManage->fetch_ad_where($id);

			echo json_encode(array(
				'ad' => $ad
			));
		}

			//set_tutorial_ban
		if($flag == "ban_this_tutorial"){
			$tut_id = $this->input->post(tutorial_id);
			$value = $this->input->post(value);

			$ban = $this->DataManage->ban_tutorial($tut_id, $value);

			if($ban){
				echo json_encode(array(
					'tutorial_banned' => 1
				));
			}
			else{
				echo json_encode(array(
					'tutorial_banned' => 0
				));
			}
		}

			//set_user_ban
		if($flag == "ban_this_user"){
			$user_id = $this->input->post(user_id);
			$value = $this->input->post(value);

			$ban = $this->DataManage->ban_user($user_id, $value);

			if($ban){
				echo json_encode(array(
					'user_banned' => 1
				));
			}
			else{
				echo json_encode(array(
					'user_banned' => 0
				));
			}
		}

			//save news
		if($flag == "save_news"){
			$data['news_title'] = $this->input->post(title);
			$data['news_content'] = $this->input->post(content);
				//$data['news_author'] = $this->input->post(title);
			$data['news_status'] = '1';

			$ban = $this->DataManage->save_news($data);

			if($ban){
				echo json_encode(array(
					'news_saved' => 1
				));
			}
			else{
				echo json_encode(array(
					'news_saved' => 0
				));
			}
		}

			//save user
		if($flag == "save_user"){
			$data['username'] = $this->input->post(username);
			$data['password'] = sha1($this->input->post(password));
			$data['status'] = '1';
			$data['role'] = '1';

			$ban = $this->DataManage->save_user($data);

			if($ban){
				echo json_encode(array(
					'user_saved' => 1
				));
			}
			else{
				echo json_encode(array(
					'user_saved' => 0
				));
			}
		}

		//update news
		if($flag == "update_news"){
			$data['news_title'] = $this->input->post(title);
			$data['news_content'] = $this->input->post(content);
				//$data['news_author'] = $this->input->post(title);
			$data['news_status'] = '1';
			$id = $this->input->post(id);

			$ban = $this->DataManage->update_news($id, $data);

			if($ban){
				echo json_encode(array(
					'news_updated' => 1
				));
			}
			else{
				echo json_encode(array(
					'news_updated' => 0
				));
			}
		}

		//update article
		if($flag == "update_article"){
			$data['article_title'] = $this->input->post(title);
			$data['article_content'] = $this->input->post(content);
				//$data['article_author'] = $this->input->post(title);
			$data['article_status'] = '1';
			$id = $this->input->post(id);

			$ban = $this->DataManage->update_article($id, $data);

			if($ban){
				echo json_encode(array(
					'article_updated' => 1
				));
			}
			else{
				echo json_encode(array(
					'article_updated' => 0
				));
			}
		}

		//update admin
		if($flag == "update_user"){
			$data['username'] = $this->input->post(username);
			$data['password'] = sha1($this->input->post(password));
			$data['status'] = '1';
			$data['role'] = '1';
			$id = $this->input->post(id);

			$ban = $this->DataManage->update_user($id, $data);

			if($ban){
				echo json_encode(array(
					'user_updated' => 1
				));
			}
			else{
				echo json_encode(array(
					'user_updated' => 0
				));
			}
		}

		//update client
		if($flag == "update_user_client"){
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['phone'] = $this->input->post('phone');
			$pass = $this->input->post('pass');
			$id = $this->input->post('id');

			$valid = $this->DataManage->valid_update_user($id, $pass);
			if($valid > 0){
				$user = $this->DataManage->edit_member($id, $data);
				if($user){
					echo json_encode(array(
						'valid' => 1
					));
				}
				else{
					echo json_encode(array(
						'valid' => 0
					));
				}
			}else{
				echo json_encode(array(
					'valid' => 2
				));
			}
		}

		//update client pass
		if($flag == "update_user_client_pass"){
			$pass = $this->input->post('pass');
			$pass_new = $this->input->post('pass_new');
			$id = $this->input->post('id');

			$valid = $this->DataManage->valid_update_user($id, $pass);
			if($valid > 0){
				$user = $this->DataManage->update_pass($id, $pass_new);
				if($user){
					echo json_encode(array(
						'valid' => 1
					));
				}
				else{
					echo json_encode(array(
						'valid' => 0
					));
				}
			}else{
				echo json_encode(array(
					'valid' => 2
				));
			}
		}

		if($flag == "load_most_view_modules"){
			$modules = $this->DataManage->load_most_view_modules();

			echo json_encode(array(
				'list' => $modules 
			));
		}

		if($flag == "load_module_video"){
			$modules = $this->DataManage->load_module_video();

			echo json_encode(array(
				'list' => $modules 
			));
		}

		if($flag == "load_module_document"){
			$modules = $this->DataManage->load_module_document();

			echo json_encode(array(
				'list' => $modules 
			));
		}

		if($flag == "load_module_image"){
			$modules = $this->DataManage->load_module_image();

			echo json_encode(array(
				'list' => $modules 
			));
		}


		//load_fav_by_id_member
		if($flag == "load_fav_by_id_member"){
			$id_member = $this->input->post('id_member');
			$page = $this->input->post('page');
			$username = $this->input->post('username');

			$offset = ($page - 1) * 25;
			$result = $this->DataManage->load_fav_by_id_member($offset, $id_member);

			echo json_encode(array(
				'list' => $result 
			));
		}

		//save_fav
		if($flag == "save_fav"){
			$id_member = $this->input->post('id_member');
			$id_module = $this->input->post('id_module');

			$exec = $this->DataManage->save_fav($id_member, $id_module);
			if($exec){
				echo json_encode(array(
					'valid' => 1
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}

		//delete_fav
		if($flag == "delete_fav"){
			$id_member = $this->input->post('id_member');
			$id_module = $this->input->post('id_module');

			$exec = $this->DataManage->delete_fav($id_member, $id_module);
			if($exec){
				echo json_encode(array(
					'valid' => 1
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}

		//check_fav
		if($flag == "check_fav"){
			$id_member = $this->input->post('id_member');
			$id_module = $this->input->post('id_module');

			$count = $this->DataManage->check_fav($id_member, $id_module);
			if($count > 0){
				echo json_encode(array(
					'valid' => 1
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}

		//save_rating
		if($flag == "save_rating"){
			$id_member = $this->input->post('id_member');
			$id_module = $this->input->post('id_module');
			$star = $this->input->post('star');

			$exec = $this->DataManage->save_rating($id_member, $id_module, $star);
			if($exec){
				echo json_encode(array(
					'valid' => 1
				));
			}
			else{
				echo json_encode(array(
					'valid' => 0
				));
			}
		}
		
		//Edit Member
		if($flag == "edit_user_member"){
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$password = $this->input->post('password');
			$stamp = date("Y-m-d H:i:s");
			
			$this->DataManage->edit_user_member($name, $email, $phone, $password, $stamp, $id);
			
			echo json_encode(array(
				'valid' => 1
			));
		}
	}

		//Create a stupid random ID
	public function rand_string( $length ){
		$str = "";

		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
		$size = strlen( $chars );

		for( $i = 0; $i < $length; $i++ ) 
		{
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

}
?>