<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_memb extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		// $this->notifications->checkDraft();

	}

	public function index()
	{

		//PAGINATION
		$category_rows = $this->Data->get_num_rows('category');
		$config['total_rows'] = $category_rows;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);
		$config['base_url'] = base_url('category_memb/index/');
		$data['category'] = $this->Data->fetch_all_limit('category', $config['per_page'],$offset);
		
		$this->pagination->initialize($config);

		$title = "Category Management";
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/category-table-memb', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['category'] = $this->Data->fetch_member_where($id)->row();
			$title = "Edit Member";
		} else {
			$title = "Add New Category";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/category-form-memb', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	public function formedit($id='')
	{
		$judul = "Edit Category"; 
		$data['category']=$this->categori->get_by_id($id); 
		$data['footer'] = $this->footer();
		$data['sidebar'] = $this->sidebar();
		$data['header'] = $this->header($judul);
		$data['content'] = $this->load->view('pages/editcategorymemb', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	function update(){
	$id = $this->input->post('id');
	$name = $this->input->post('name');
 
	$data = array(
		'name' => $name,
		'status' => 1
	);
 
	$where = array(
		'id' => $id
	);
 
	$this->categori->update_data($where,$data,'category');
	redirect('/category_memb');
}
	
	public function tambah(){
		$name = $this->input->post('name');
 
		$data = array(
			'name' => $name,
			'status' => '1'
			);
		$this->categori->input_data($data,'category');
		redirect('/category_memb');
	}
	
	public function delCategory ($id){ //fungsi delete
		$this->load->model('categori'); //load model
		$this->categori->delCategory($id); //ngacir ke fungsi delTransaksi
		redirect('/category_memb'); //redirect
	 
	}
 
	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar _memb', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

}