<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboardmemb extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		// $this->notifications->checkDraft();
	}
 
	public function index()
	{
		$data['datas']= $this->admin->getMember('member');
		$data['countmember']=$this->admin->get_count('member');
		$data['countmodule']=$this->admin->get_countmodule('modules');
		$data['countarticle']=$this->admin->get_countarticle('articles');
		$data['countads']=$this->admin->get_countads('ads');
		$data['datasa']= $this->admin->getModules('modules');
		$data['datasb']= $this->admin->getArticles('articles');
		$data['datasc']= $this->admin->getAds('ads');
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header();
		$data['content'] = $this->load->view('pages/dashboardmemb', $data, true);
		$this->load->view('templates/main',$data);
	}

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar _memb', $data, true);
	}

	public function header()
	{
		$data['title'] = "Dashboard";
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

}