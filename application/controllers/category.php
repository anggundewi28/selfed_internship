<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		$this->authent->checkLogin();
		// $this->notifications->checkDraft();

	}

	public function index()
	{

		//PAGINATION
		$category_rows = $this->DataManage->get_num_rows('category');

		$config['base_url'] = base_url('category/index/');
		$config['total_rows'] = $category_rows;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);

		$data['category'] = $this->DataManage->fetch_all_limit('category', $config['per_page'],$offset);
		
		$this->pagination->initialize($config);

		$title = "Category Management";
		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/category-table', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['category'] = $this->DataManage->fetch_member_where($id)->row();
			$title = "Edit Member";
		} else {
			$title = "Add New Category";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/category-form', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	public function formedit($id='')
	{
		$judul = "Edit Category"; 
		$data['category']=$this->categori->get_by_id($id); 
		$data['footer'] = $this->footer();
		$data['sidebar'] = $this->sidebar();
		$data['header'] = $this->header($judul);
		$data['content'] = $this->load->view('pages/editcategory', $data, true);
		$this->load->view('templates/main', $data);
	}
	
	function update(){
	$id = $this->input->post('id');
	$name = $this->input->post('name');
 
	$data = array(
		'name' => $name,
		'status' => 1
	);
 
	$where = array(
		'id' => $id
	);
 
	$this->categori->update_data($where,$data,'category');
	redirect('/category');
}
	
	public function tambah(){
		$name = $this->input->post('name');
 
		$data = array(
			'name' => $name,
			'status' => '1'
			);
		$this->categori->input_data($data,'category');
		redirect('/category');
	}
	
	public function delCategory ($id){ //fungsi delete
		$this->load->model('categori'); //load model
		$this->categori->delCategory($id); //ngacir ke fungsi delTransaksi
		redirect('/category'); //redirect
	 
	}
 
	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

}