<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ads_memb extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();

		// $this->notifications->checkDraft();

	}
 
	public function index()
	{
		//PAGINATION
		$ad_rows = $this->DataManage->get_num_rows('ads');
		$config['total_rows'] = $ad_rows;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);
		$config['base_url'] = base_url('ads_memb/index/');
		$data['ads'] = $this->DataManage->fetch_all_limit('ads', $config['per_page'],$offset);
		
		$this->pagination->initialize($config);

		$title = "Ads Management";

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/ads-table-memb', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['ads'] = $this->Data->fetch_ad_where($id)->row();
			$title = "Edit Ad";
		} else {
			$title = "Add New Ad";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar'] = $this->sidebar();
		$data['header'] = $this->header($title);
		$data['content'] = $this->load->view('pages/ads-form-memb', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar _memb', $data, true);
	}

	public function header($title)
	{
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

	public function add()
	{
		$config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'jpg|jpeg|png|PNG|JPG|JPEG';
        $config['max_size']             = 5000;

        $this->load->library('upload', $config);
    
        $this->upload->do_upload('ads_image');
        $image_upload = $this->upload->data();

		$ads_image = $_FILES["ads_image"]["name"];
		
		$id = $this->input->post('id');

		if( is_numeric($id) && empty($ads_image)) {

			$data = array(
				'ad_name' => $this->input->post('name'),
				'ad_url' => $this->input->post('url'),
				'ad_status' => '1'
			);	
		
		} 
		else {

			$data = array(
				'ad_name' => $this->input->post('name'),
				'ad_url' => $this->input->post('url'),
				'ad_path' => $config['upload_path'].$ads_image,
				'ad_status' => '1'
			);
		}

		if (is_numeric($id)) {
			$query = $this->DataManage->update_ad($id, $data);
		} else {
			$query = $this->DataManage->save_ad($data);	
		}
		
		if($query) {
			redirect('/ads_memb');
		}
	}

	public function delete_ad($id)
	{
		$del = $this->DataManage->delete_ad($id);
		if( $del ) {
			$this->session->set_flashdata('delete-success', 'Ad Deleted');
		} else {
			$this->session->set_flashdata('delete-fail', 'Failed to Delete Ad');
		}

		redirect('/ads_memb');
		
	}


}