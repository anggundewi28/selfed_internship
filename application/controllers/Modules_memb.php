<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules_memb extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();
		// $this->notifications->checkDraft();
 
	}

	public function index()
	{
		//PAGINATION
		$module_rows = $this->modulememb->get_num_rows('modules');
		$keyword = $this->input->post('keyword');
		$data['modules']=$this->modulememb->get_module_keyword($keyword);
		$config['total_rows'] = $module_rows;
		$config['base_url'] = base_url('modules_memb/index/');
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
		$offset = $this->uri->segment(3);

		$data['modules'] = $this->modulememb->fetch_all_limit('modules', $config['per_page'],$offset);
		$this->pagination->initialize($config);

		$title = "Module Management";
		$data['footer'] = $this->footer();
		$data['sidebar'] = $this->sidebar();
		$data['header'] = $this->header($title);
		$data['content'] = $this->load->view('pages/modules-table-memb', $data, true);
		$this->load->view('templates/main', $data);
	}


	public function formedit($id='')
	{
		$judul = "Edit Modules"; 
		$data['modules']=$this->module->get_by_id($id); 
		$data['footer'] = $this->footer();
		$data['sidebar'] = $this->sidebar();
		$data['header'] = $this->header($judul);
		$data['content'] = $this->load->view('pages/editmodulememb', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function upload()
	{
		$config = array(
			'upload_path'=>'./modulesview/',
			'allowed_types'=>'gif|jpg|png|jpeg|pdf|mp4|PNG|JPG|JPEG|PDF|MP4',
			'max_size'=>2048000,
			'max_width'=>1024,
			'max_height'=>768,
			);
		$this->load->library('upload',$config);
		$this->upload->do_upload('modules_image');

		$finfo = $this->upload->data(
		);
		$nama_foto = $finfo['file_name'];

		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$pembayaran = $this->input->post('pembayaran');

		//------------------------------------------

		$data_module = array(
			'title' => $title,
			'description' => $description,
			'pembayaran' => $pembayaran,
			'path'	=> $nama_foto,
			'status' => '1'	
			);

		$config2 = array(
			'source_image'=>'modulesview/'.$nama_foto,
			'image_library'=>'gd2',
			'new_image'=>'modulesview/',
			'maintain_ratio'=>true,
			'width'=>150,
			'height'=>200
		);
		$this->load->library('image_lib',$config2);
		$this->image_lib->resize();
		$this->modulememb->tambah_data($data_module);
		$this->session->set_flashdata('alert','Insert Data Successfully !');
		redirect('/modules_memb');
	}

	public function update($id=''){
		$config = array(
			'upload_path'=>'./modulesview/',
			'allowed_types'=>'gif|jpg|png|jpeg|pdf|mp4|PNG|JPG|JPEG|PDF|MP4',
			'max_size'=>2048000,
			'max_width'=>1024,
			'max_height'=>768,
			);

		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$pembayaran = $this->input->post('pembayaran');
		$data = array('id'=>$id);
		$foto = $this->db->get_where('modules',$data);
		
		if($foto->num_rows()>0){
		$pros=$foto->row();
		$name=$pros->path;
		
		if(file_exists($lok=FCPATH.'./modulesview/'.$name)){
			unlink($lok);
		}
		if(file_exists($lok=FCPATH.'./modulesview/'.$name)){
			unlink($lok);
		}}

		$this->load->library('upload',$config);
		
		if($this->upload->do_upload('modules_image')){

		$finfo = $this->upload->data();
		$nama_foto = $finfo['file_name'];

		$data_module = array(
							'title' => $title,
							'description' => $description,
							'pembayaran' => $pembayaran,
							'path'	=> $nama_foto,
							'status' => '1'
							);

		$config2 = array(
				'source_image'=>'./modulesview/'.$nama_foto,
				'image_library'=>'gd2',
				'new_image'=>'./modulesview/',
				'maintain_ratio'=>true,
				'width'=>150,
				'height'=>200
			);
		
		$this->load->library('image_lib',$config2);
		$this->image_lib->resize();		
		
		}else{
		$data_module = array(
			'title' => $title,
			'description' => $description,
			'pembayaran' => $pembayaran,
			'status' => '1'
			);

		}
		
		$this->modulememb->edit_data($data,$data_module);
		redirect('/modules_memb');
	}

	public function sidebar()
	{
		$data = array();
		return $this->load->view('templates/sidebar _memb', $data, true);
	}

	public function form($id = '')
	{
		if(is_numeric($id)) {
			$data['module'] = $this->DataManage->fetch_modules_where($id)->row();
			$title = "Edit Modules";
		} else {
			$title = "Add New Modules";	
		}

		$data['footer'] = $this->footer();
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/modules-form-memb', $data, true);
		$this->load->view('templates/main', $data);
	}

	public function header($title)
	{ 
		$data['title'] = $title;
		return $this->load->view('templates/header', $data, true);
	}

	public function footer()
	{
		$data = array();
		return $this->load->view('templates/footer', $data, true);
	}

	public function delModule ($id){ //fungsi delete
		$this->load->model('module'); //load model
		$this->modulememb->delModule($id); //ngacir ke fungsi delTransaksi
		redirect('/modules_memb'); //redirect
	 
	}

	public function search()
	{
		$title = "Module Management";
		$keyword = $this->input->post('keyword');
		$data['modules']=$this->modulememb->get_module_keyword($keyword);
		$data['sidebar']= $this->sidebar();
		$data['header']= $this->header($title);
		$data['content'] = $this->load->view('pages/modules-table-memb', $data, true);
		$data['footer'] = $this->footer();
		
		$this->load->view('templates/main', $data);
	}
}